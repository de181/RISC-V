// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 23.06.2022 at 09:16
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


`ifndef ISS_INTERRUPTS_ASSERTIONS
`define ISS_INTERRUPTS_ASSERTIONS


// Assertions
`include "ISS_Interrupts_timing.sv"
import top_level_types::*;
import iss_interrupts_types::*;


module ISS_Interrupts_verification(reset);
  input reset;

  `include "ISS_Interrupts_properties.sv"

  default clocking default_clk @(posedge `clk); endclocking


  reset_a: assert property (reset_p);


  ECALL_DONE_to_FETCH_REQ_a: assert property(disable iff(reset) ECALL_DONE_to_FETCH_REQ_p);
  ECALL_REQ_to_ECALL_DONE_a: assert property(disable iff(reset) ECALL_REQ_to_ECALL_DONE_p);
  FETCH_DONE_to_ECALL_REQ_a: assert property(disable iff(reset) FETCH_DONE_to_ECALL_REQ_p);
  FETCH_DONE_to_FETCH_REQ_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_p);
  FETCH_DONE_to_FETCH_REQ_1_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_1_p);
  FETCH_DONE_to_FETCH_REQ_10_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_10_p);
  FETCH_DONE_to_FETCH_REQ_11_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_11_p);
  FETCH_DONE_to_FETCH_REQ_12_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_12_p);
  FETCH_DONE_to_FETCH_REQ_13_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_13_p);
  FETCH_DONE_to_FETCH_REQ_14_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_14_p);
  FETCH_DONE_to_FETCH_REQ_15_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_15_p);
  FETCH_DONE_to_FETCH_REQ_16_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_16_p);
  FETCH_DONE_to_FETCH_REQ_17_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_17_p);
  FETCH_DONE_to_FETCH_REQ_18_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_18_p);
  FETCH_DONE_to_FETCH_REQ_19_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_19_p);
  FETCH_DONE_to_FETCH_REQ_2_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_2_p);
  FETCH_DONE_to_FETCH_REQ_20_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_20_p);
  FETCH_DONE_to_FETCH_REQ_21_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_21_p);
  FETCH_DONE_to_FETCH_REQ_22_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_22_p);
  FETCH_DONE_to_FETCH_REQ_23_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_23_p);
  FETCH_DONE_to_FETCH_REQ_24_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_24_p);
  FETCH_DONE_to_FETCH_REQ_25_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_25_p);
  FETCH_DONE_to_FETCH_REQ_26_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_26_p);
  FETCH_DONE_to_FETCH_REQ_27_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_27_p);
  FETCH_DONE_to_FETCH_REQ_28_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_28_p);
  FETCH_DONE_to_FETCH_REQ_29_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_29_p);
  FETCH_DONE_to_FETCH_REQ_3_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_3_p);
  FETCH_DONE_to_FETCH_REQ_30_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_30_p);
  FETCH_DONE_to_FETCH_REQ_31_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_31_p);
  FETCH_DONE_to_FETCH_REQ_32_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_32_p);
  FETCH_DONE_to_FETCH_REQ_33_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_33_p);
  FETCH_DONE_to_FETCH_REQ_34_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_34_p);
  FETCH_DONE_to_FETCH_REQ_35_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_35_p);
  FETCH_DONE_to_FETCH_REQ_36_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_36_p);
  FETCH_DONE_to_FETCH_REQ_4_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_4_p);
  FETCH_DONE_to_FETCH_REQ_5_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_5_p);
  FETCH_DONE_to_FETCH_REQ_6_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_6_p);
  FETCH_DONE_to_FETCH_REQ_7_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_7_p);
  FETCH_DONE_to_FETCH_REQ_8_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_8_p);
  FETCH_DONE_to_FETCH_REQ_9_a: assert property(disable iff(reset) FETCH_DONE_to_FETCH_REQ_9_p);
  FETCH_DONE_to_LOAD_REQ_a: assert property(disable iff(reset) FETCH_DONE_to_LOAD_REQ_p);
  FETCH_DONE_to_STORE_a: assert property(disable iff(reset) FETCH_DONE_to_STORE_p);
  FETCH_REQ_to_FETCH_DONE_a: assert property(disable iff(reset) FETCH_REQ_to_FETCH_DONE_p);
  LOAD_DONE_to_FETCH_REQ_a: assert property(disable iff(reset) LOAD_DONE_to_FETCH_REQ_p);
  LOAD_REQ_to_LOAD_DONE_a: assert property(disable iff(reset) LOAD_REQ_to_LOAD_DONE_p);
  STORE_to_FETCH_REQ_a: assert property(disable iff(reset) STORE_to_FETCH_REQ_p);


  ECALL_DONE_wait_a: assert property(disable iff(reset) ECALL_DONE_wait_p);
  ECALL_REQ_wait_a: assert property(disable iff(reset) ECALL_REQ_wait_p);
  FETCH_DONE_wait_a: assert property(disable iff(reset) FETCH_DONE_wait_p);
  FETCH_REQ_wait_a: assert property(disable iff(reset) FETCH_REQ_wait_p);
  LOAD_DONE_wait_a: assert property(disable iff(reset) LOAD_DONE_wait_p);
  LOAD_REQ_wait_a: assert property(disable iff(reset) LOAD_REQ_wait_p);
  STORE_wait_a: assert property(disable iff(reset) STORE_wait_p);


endmodule


`endif
