// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 23.06.2022 at 09:16
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------

// Required terminology


sequence hold(l, e);
  l == e;
endsequence
