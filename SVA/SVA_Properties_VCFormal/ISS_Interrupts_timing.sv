// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 23.06.2022 at 09:16
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


`ifndef ISS_INTERRUPTS_TIMING
`define ISS_INTERRUPTS_TIMING


// Clock and reset


// Assign the model correct name of the reset signal used in it
`define rst ISS_Interrupts.rst
// Assign the model correct name of the clock signal used in it
`define clk ISS_Interrupts.clk


`endif
