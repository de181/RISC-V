// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 23.06.2022 at 09:16
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


`ifndef ISS_INTERRUPTS_SYNCHRONIZATION
`define ISS_INTERRUPTS_SYNCHRONIZATION


// Sync and notify signals


`define ecall_isa_Port_sync ISS_Interrupts.ecall_isa_Port_sync

`define fromMemoryPort_sync ISS_Interrupts.fromMemoryPort_sync

`define isa_ecall_Port_sync ISS_Interrupts.isa_ecall_Port_sync

`define toMemoryPort_sync ISS_Interrupts.toMemoryPort_sync

`define ecall_isa_Port_notify ISS_Interrupts.ecall_isa_Port_notify

`define fromMemoryPort_notify ISS_Interrupts.fromMemoryPort_notify

`define isa_ecall_Port_notify ISS_Interrupts.isa_ecall_Port_notify

`define toMemoryPort_notify ISS_Interrupts.toMemoryPort_notify

`define toRegsPort_notify ISS_Interrupts.toRegsPort_notify


`endif
