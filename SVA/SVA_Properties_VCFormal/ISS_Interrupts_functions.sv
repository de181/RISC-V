// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 23.06.2022 at 09:16
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


`ifndef ISS_INTERRUPTS_FUNCTIONS
`define ISS_INTERRUPTS_FUNCTIONS


// Local functions


function e_ALUfuncType getALUfunc(e_InstrType_Complete instr);
  if ((((((((((((instr == INSTR_ADD) || (instr == INSTR_ADDI)) || (instr == INSTR_LB)) || (instr == INSTR_LH)) || (instr == INSTR_LW)) || (instr == INSTR_LBU)) || (instr == INSTR_LHU)) || (instr == INSTR_SB)) || (instr == INSTR_SH)) || (instr == INSTR_SW)) || (instr == INSTR_AUIPC)))
    return ALU_ADD;
  else if ((((instr == INSTR_SUB) || (instr == INSTR_BEQ)) || (instr == INSTR_BNE)))
    return ALU_SUB;
  else if (((instr == INSTR_SLL) || (instr == INSTR_SLLI)))
    return ALU_SLL;
  else if (((((instr == INSTR_SLT) || (instr == INSTR_SLTI)) || (instr == INSTR_BLT)) || (instr == INSTR_BGE)))
    return ALU_SLT;
  else if (((((instr == INSTR_SLTU) || (instr == INSTR_SLTUI)) || (instr == INSTR_BLTU)) || (instr == INSTR_BGEU)))
    return ALU_SLTU;
  else if (((instr == INSTR_XOR) || (instr == INSTR_XORI)))
    return ALU_XOR;
  else if (((instr == INSTR_SRL) || (instr == INSTR_SRLI)))
    return ALU_SRL;
  else if (((instr == INSTR_SRA) || (instr == INSTR_SRAI)))
    return ALU_SRA;
  else if (((((instr == INSTR_OR) || (instr == INSTR_ORI)) || (instr == INSTR_CSRRS)) || (instr == INSTR_CSRRSI)))
    return ALU_OR;
  else if (((instr == INSTR_AND) || (instr == INSTR_ANDI)))
    return ALU_AND;
  else if (((instr == INSTR_JALR) || (instr == INSTR_JAL)))
    return ALU_X;
  else if ((instr == INSTR_LUI))
    return ALU_COPY1;
  else if (((instr == INSTR_CSRRW) || (instr == INSTR_CSRRWI)))
    return ALU_CSRRW;
  else if (((instr == INSTR_CSRRC) || (instr == INSTR_CSRRCI)))
    return ALU_CSRRC;
  else
    return ALU_X;
endfunction

function bit[31:0] getALUresult(e_ALUfuncType aluFunction, bit[31:0] operand1, bit[31:0] operand2);
  if ((aluFunction == ALU_ADD))
    return (operand1 + operand2);
  else if ((aluFunction == ALU_SUB))
    return (operand1 - operand2);
  else if ((aluFunction == ALU_AND))
    return (operand1 & operand2);
  else if ((aluFunction == ALU_OR))
    return (operand1 | operand2);
  else if ((aluFunction == ALU_XOR))
    return (operand1 ^ operand2);
  else if ((aluFunction == ALU_SLT) && (signed'(32'(operand1)) < signed'(32'(operand2))))
    return 1;
  else if ((aluFunction == ALU_SLT))
    return 0;
  else if ((aluFunction == ALU_SLTU) && (operand1 < operand2))
    return 1;
  else if ((aluFunction == ALU_SLTU))
    return 0;
  else if ((aluFunction == ALU_SLL))
    return (operand1 << (operand2 & 31));
  else if ((aluFunction == ALU_SRA))
    return unsigned'(32'((signed'(32'(operand1)) >>> signed'(32'((operand2 & 31))))));
  else if ((aluFunction == ALU_SRL))
    return (operand1 >> (operand2 & 31));
  else if ((aluFunction == ALU_COPY1))
    return operand2;
  else if ((aluFunction == ALU_CSRRW))
    return operand1;
  else if ((aluFunction == ALU_CSRRC))
    return (operand2 & ((operand1 * 4294967295) - 1));
  else
    return 0;
endfunction

function bit[31:0] getALUresult_U(bit[31:0] encodedInstr, bit[31:0] pcReg, bit[31:0] imm);
  if ((getInstrType(encodedInstr) == INSTR_LUI))
    return imm;
  else
    return (pcReg + imm);
endfunction

function bit[31:0] getBranchresult(bit[31:0] encodedInstr, bit[31:0] aluResult, bit[31:0] pcReg);
  if (((getInstrType(encodedInstr) == INSTR_BEQ) && (aluResult == 0)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BNE) && (aluResult != 0)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BLT) && (aluResult == 1)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BGE) && (aluResult == 0)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BLTU) && (aluResult == 1)))
    return (pcReg + getImmediate(encodedInstr));
  else if (((getInstrType(encodedInstr) == INSTR_BGEU) && (aluResult == 0)))
    return (pcReg + getImmediate(encodedInstr));
  else
    return (pcReg + 4);
endfunction

function bit[31:0] getCSR(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit[31:0] encodedInstr);
  if (((encodedInstr >> 20) == 768))
    return csrfile_mstatus;
  else if (((encodedInstr >> 20) == 772))
    return csrfile_mie;
  else if (((encodedInstr >> 20) == 773))
    return csrfile_mtvec;
  else if (((encodedInstr >> 20) == 833))
    return csrfile_mepc;
  else if (((encodedInstr >> 20) == 834))
    return csrfile_mcause;
  else if (((encodedInstr >> 20) == 832))
    return csrfile_mscratch;
  else if (((encodedInstr >> 20) == 769))
    return csrfile_misa;
  else if (((encodedInstr >> 20) == 835))
    return csrfile_mtval;
  else
    return 0;
endfunction

function e_EncType getEncType(bit[31:0] encodedInstr);
  if (((encodedInstr & 127) == 51))
    return ENC_R;
  else if (((encodedInstr & 127) == 19))
    return ENC_I_I;
  else if (((encodedInstr & 127) == 3))
    return ENC_I_L;
  else if (((encodedInstr & 127) == 103))
    return ENC_I_J;
  else if (((encodedInstr & 127) == 15))
    return ENC_I_M;
  else if (((encodedInstr & 127) == 115))
    return ENC_I_S;
  else if (((encodedInstr & 127) == 35))
    return ENC_S;
  else if (((encodedInstr & 127) == 99))
    return ENC_B;
  else if ((((encodedInstr & 127) == 55) || ((encodedInstr & 127) == 23)))
    return ENC_U;
  else if (((encodedInstr & 127) == 111))
    return ENC_J;
  else
    return ENC_ERR;
endfunction

function bit[31:0] getImmediate(bit[31:0] encodedInstr);
  if (((((((encodedInstr & 127) == 19) || ((encodedInstr & 127) == 3)) || ((encodedInstr & 127) == 15)) || ((encodedInstr & 127) == 115)) || ((encodedInstr & 127) == 103)) && (((encodedInstr >> 31) & 1) == 0))
    return (encodedInstr >> 20);
  else if (((((((encodedInstr & 127) == 19) || ((encodedInstr & 127) == 3)) || ((encodedInstr & 127) == 15)) || ((encodedInstr & 127) == 115)) || ((encodedInstr & 127) == 103)))
    return (unsigned'(32'(-4096)) | (encodedInstr >> 20));
  else if (((encodedInstr & 127) == 35) && (((encodedInstr >> 31) & 1) == 0))
    return (((encodedInstr >> 20) & 4064) | ((encodedInstr >> 7) & 31));
  else if (((encodedInstr & 127) == 35))
    return (unsigned'(32'(-4096)) | (((encodedInstr >> 20) & 4064) | ((encodedInstr >> 7) & 31)));
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 31) & 1) == 0))
    return ((((encodedInstr << 4) & 2048) | ((encodedInstr >> 20) & 2016)) | ((encodedInstr >> 7) & 30));
  else if (((encodedInstr & 127) == 99))
    return (unsigned'(32'(-4096)) | ((((encodedInstr << 4) & 2048) | ((encodedInstr >> 20) & 2016)) | ((encodedInstr >> 7) & 30)));
  else if ((((encodedInstr & 127) == 55) || ((encodedInstr & 127) == 23)))
    return (encodedInstr & unsigned'(32'(-4096)));
  else if (((encodedInstr & 127) == 111) && (((encodedInstr >> 31) & 1) == 0))
    return (((encodedInstr & 1044480) | ((encodedInstr >> 9) & 2048)) | ((encodedInstr >> 20) & 2046));
  else if (((encodedInstr & 127) == 111))
    return (unsigned'(32'(-1048576)) | (((encodedInstr & 1044480) | ((encodedInstr >> 9) & 2048)) | ((encodedInstr >> 20) & 2046)));
  else
    return 0;
endfunction

function e_InstrType_Complete getInstrType(bit[31:0] encodedInstr);
  if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 0) && (((encodedInstr >> 25) & 127) == 0))
    return INSTR_ADD;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 0) && (((encodedInstr >> 25) & 127) == 32))
    return INSTR_SUB;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 0))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 1))
    return INSTR_SLL;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 2))
    return INSTR_SLT;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 3))
    return INSTR_SLTU;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 4))
    return INSTR_XOR;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 5) && (((encodedInstr >> 25) & 127) == 0))
    return INSTR_SRL;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 5) && (((encodedInstr >> 25) & 127) == 32))
    return INSTR_SRA;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 5))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 6))
    return INSTR_OR;
  else if (((encodedInstr & 127) == 51) && (((encodedInstr >> 12) & 7) == 7))
    return INSTR_AND;
  else if (((encodedInstr & 127) == 51))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 0))
    return INSTR_ADDI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 1))
    return INSTR_SLLI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 2))
    return INSTR_SLTI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 3))
    return INSTR_SLTUI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 4))
    return INSTR_XORI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 5) && (((encodedInstr >> 25) & 127) == 0))
    return INSTR_SRLI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 5) && (((encodedInstr >> 25) & 127) == 32))
    return INSTR_SRAI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 5))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 6))
    return INSTR_ORI;
  else if (((encodedInstr & 127) == 19) && (((encodedInstr >> 12) & 7) == 7))
    return INSTR_ANDI;
  else if (((encodedInstr & 127) == 19))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 0))
    return INSTR_LB;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 1))
    return INSTR_LH;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 2))
    return INSTR_LW;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 4))
    return INSTR_LBU;
  else if (((encodedInstr & 127) == 3) && (((encodedInstr >> 12) & 7) == 5))
    return INSTR_LHU;
  else if (((encodedInstr & 127) == 3))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 103))
    return INSTR_JALR;
  else if (((encodedInstr & 127) == 15) && (((encodedInstr >> 12) & 7) == 0))
    return INSTR_FENCE;
  else if (((encodedInstr & 127) == 15) && (((encodedInstr >> 12) & 7) == 1))
    return INSTR_FENCEI;
  else if (((encodedInstr & 127) == 15))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 0) && ((encodedInstr >> 20) == 0))
    return INSTR_ECALL;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 0) && ((encodedInstr >> 20) == 1))
    return INSTR_EBREAK;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 0) && ((encodedInstr >> 20) == 770))
    return INSTR_MRET;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 0))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 1))
    return INSTR_CSRRW;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 2))
    return INSTR_CSRRS;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 3))
    return INSTR_CSRRC;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 5))
    return INSTR_CSRRWI;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 6))
    return INSTR_CSRRSI;
  else if (((encodedInstr & 127) == 115) && (((encodedInstr >> 12) & 7) == 7))
    return INSTR_CSRRCI;
  else if (((encodedInstr & 127) == 115))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 35) && (((encodedInstr >> 12) & 7) == 0))
    return INSTR_SB;
  else if (((encodedInstr & 127) == 35) && (((encodedInstr >> 12) & 7) == 1))
    return INSTR_SH;
  else if (((encodedInstr & 127) == 35) && (((encodedInstr >> 12) & 7) == 2))
    return INSTR_SW;
  else if (((encodedInstr & 127) == 35))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 0))
    return INSTR_BEQ;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 1))
    return INSTR_BNE;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 4))
    return INSTR_BLT;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 5))
    return INSTR_BGE;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 6))
    return INSTR_BLTU;
  else if (((encodedInstr & 127) == 99) && (((encodedInstr >> 12) & 7) == 7))
    return INSTR_BGEU;
  else if (((encodedInstr & 127) == 99))
    return INSTR_UNKNOWN;
  else if (((encodedInstr & 127) == 55))
    return INSTR_LUI;
  else if (((encodedInstr & 127) == 23))
    return INSTR_AUIPC;
  else if (((encodedInstr & 127) == 111))
    return INSTR_JAL;
  else
    return INSTR_UNKNOWN;
endfunction

function e_ME_MaskType getMemoryMask(e_InstrType_Complete instr);
  if (((instr == INSTR_LW) || (instr == INSTR_SW)))
    return MT_W;
  else if (((instr == INSTR_LH) || (instr == INSTR_SH)))
    return MT_H;
  else if (((instr == INSTR_LB) || (instr == INSTR_SB)))
    return MT_B;
  else if ((instr == INSTR_LHU))
    return MT_HU;
  else if ((instr == INSTR_LBU))
    return MT_BU;
  else
    return MT_X;
endfunction

function bit[31:0] updateMCAUSE(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit excFlag);
  if (excFlag)
    return csrfile_mcause;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 2048) != 0) && ((csrfile_mip & 2048) != 0)))
    return -2147483637;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 8) != 0) && ((csrfile_mip & 8) != 0)))
    return -2147483645;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 128) != 0) && ((csrfile_mip & 128) != 0)))
    return -2147483641;
  else if (((csrfile_mstatus & 8) != 0))
    return csrfile_mcause;
  else
    return csrfile_mcause;
endfunction

function bit[31:0] updateMEPC(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit[31:0] pcReg, bit excFlag);
  if (excFlag)
    return csrfile_mepc;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 2048) != 0) && ((csrfile_mip & 2048) != 0)))
    return pcReg;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 8) != 0) && ((csrfile_mip & 8) != 0)))
    return pcReg;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 128) != 0) && ((csrfile_mip & 128) != 0)))
    return pcReg;
  else if (((csrfile_mstatus & 8) != 0))
    return csrfile_mepc;
  else
    return csrfile_mepc;
endfunction

function bit[31:0] updateMSTATUS(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit excFlag);
  if (excFlag)
    return (csrfile_mstatus & unsigned'(32'(-9)));
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 2048) != 0) && ((csrfile_mip & 2048) != 0)))
    return (csrfile_mstatus & unsigned'(32'(-9)));
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 8) != 0) && ((csrfile_mip & 8) != 0)))
    return (csrfile_mstatus & unsigned'(32'(-9)));
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 128) != 0) && ((csrfile_mip & 128) != 0)))
    return (csrfile_mstatus & unsigned'(32'(-9)));
  else if (((csrfile_mstatus & 8) != 0))
    return csrfile_mstatus;
  else
    return csrfile_mstatus;
endfunction

function bit[31:0] updatePC(bit[31:0] csrfile_mcause, bit[31:0] csrfile_mepc, bit[31:0] csrfile_mie, bit[31:0] csrfile_mip, bit[31:0] csrfile_misa, bit[31:0] csrfile_mscratch, bit[31:0] csrfile_mstatus, bit[31:0] csrfile_mtval, bit[31:0] csrfile_mtvec, bit[31:0] pcReg, bit excFlag);
  if (excFlag)
    return csrfile_mtvec;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 2048) != 0) && ((csrfile_mip & 2048) != 0)))
    return csrfile_mtvec;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 8) != 0) && ((csrfile_mip & 8) != 0)))
    return csrfile_mtvec;
  else if (((csrfile_mstatus & 8) != 0) && (((csrfile_mie & 128) != 0) && ((csrfile_mip & 128) != 0)))
    return csrfile_mtvec;
  else if (((csrfile_mstatus & 8) != 0))
    return pcReg;
  else
    return pcReg;
endfunction


`endif
