// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:51
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------

// Required terminology


sequence hold(l, e);
  (l===e, l=e);
endsequence
