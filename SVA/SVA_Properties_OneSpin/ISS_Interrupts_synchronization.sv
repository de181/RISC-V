// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:51
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


// Sync and notify signals


function bit ecall_isa_Port_sync;
  ecall_isa_Port_sync = ISS_Interrupts.ecall_isa_Port_sync;
endfunction

function bit fromMemoryPort_sync;
  fromMemoryPort_sync = ISS_Interrupts.fromMemoryPort_sync;
endfunction

function bit isa_ecall_Port_sync;
  isa_ecall_Port_sync = ISS_Interrupts.isa_ecall_Port_sync;
endfunction

function bit toMemoryPort_sync;
  toMemoryPort_sync = ISS_Interrupts.toMemoryPort_sync;
endfunction

function bit ecall_isa_Port_notify;
  ecall_isa_Port_notify = ISS_Interrupts.ecall_isa_Port_notify;
endfunction

function bit fromMemoryPort_notify;
  fromMemoryPort_notify = ISS_Interrupts.fromMemoryPort_notify;
endfunction

function bit isa_ecall_Port_notify;
  isa_ecall_Port_notify = ISS_Interrupts.isa_ecall_Port_notify;
endfunction

function bit toMemoryPort_notify;
  toMemoryPort_notify = ISS_Interrupts.toMemoryPort_notify;
endfunction

function bit toRegsPort_notify;
  toRegsPort_notify = ISS_Interrupts.toRegsPort_notify;
endfunction
