// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:51
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


// Data signals


function bit ecall_isa_Port_sig;
  ecall_isa_Port_sig = ISS_Interrupts.ecall_isa_Port;
endfunction

function bit[31:0] fromMemoryPort_sig_loadedData;
  fromMemoryPort_sig_loadedData = ISS_Interrupts.fromMemoryPort.loadedData;
endfunction

`define fromRegsPort_sig ISS_Interrupts.fromRegsPort

function bit[31:0] fromRegsPort_sig_0;
  fromRegsPort_sig_0 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_1;
  fromRegsPort_sig_1 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_2;
  fromRegsPort_sig_2 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_3;
  fromRegsPort_sig_3 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_4;
  fromRegsPort_sig_4 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_5;
  fromRegsPort_sig_5 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_6;
  fromRegsPort_sig_6 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_7;
  fromRegsPort_sig_7 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_8;
  fromRegsPort_sig_8 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_9;
  fromRegsPort_sig_9 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_10;
  fromRegsPort_sig_10 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_11;
  fromRegsPort_sig_11 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_12;
  fromRegsPort_sig_12 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_13;
  fromRegsPort_sig_13 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_14;
  fromRegsPort_sig_14 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_15;
  fromRegsPort_sig_15 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_16;
  fromRegsPort_sig_16 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_17;
  fromRegsPort_sig_17 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_18;
  fromRegsPort_sig_18 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_19;
  fromRegsPort_sig_19 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_20;
  fromRegsPort_sig_20 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_21;
  fromRegsPort_sig_21 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_22;
  fromRegsPort_sig_22 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_23;
  fromRegsPort_sig_23 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_24;
  fromRegsPort_sig_24 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_25;
  fromRegsPort_sig_25 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_26;
  fromRegsPort_sig_26 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_27;
  fromRegsPort_sig_27 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_28;
  fromRegsPort_sig_28 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_29;
  fromRegsPort_sig_29 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_30;
  fromRegsPort_sig_30 = 0;
endfunction

function bit[31:0] fromRegsPort_sig_31;
  fromRegsPort_sig_31 = 0;
endfunction

function bit isa_ecall_Port_sig;
  isa_ecall_Port_sig = ISS_Interrupts.isa_ecall_Port;
endfunction

function bit[31:0] mip_isa_Port_sig;
  mip_isa_Port_sig = ISS_Interrupts.mip_isa_Port;
endfunction

function bit[31:0] toMemoryPort_sig_addrIn;
  toMemoryPort_sig_addrIn = ISS_Interrupts.toMemoryPort.addrIn;
endfunction

function bit[31:0] toMemoryPort_sig_dataIn;
  toMemoryPort_sig_dataIn = ISS_Interrupts.toMemoryPort.dataIn;
endfunction

function e_ME_MaskType toMemoryPort_sig_mask;
  toMemoryPort_sig_mask = ISS_Interrupts.toMemoryPort.mask;
endfunction

function e_ME_AccessType toMemoryPort_sig_req;
  toMemoryPort_sig_req = ISS_Interrupts.toMemoryPort.req;
endfunction

function bit[31:0] toRegsPort_sig_dst;
  toRegsPort_sig_dst = ISS_Interrupts.toRegsPort.dst;
endfunction

function bit[31:0] toRegsPort_sig_dstData;
  toRegsPort_sig_dstData = ISS_Interrupts.toRegsPort.dstData;
endfunction
