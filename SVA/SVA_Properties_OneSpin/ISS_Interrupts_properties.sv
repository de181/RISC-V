// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:51
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------

// Properties


`include "ISS_Interrupts_functions.sv"
`include "ISS_Interrupts_synchronization.sv"
`include "ISS_Interrupts_datapath.sv"
`include "ISS_Interrupts_registers.sv"
`include "ISS_Interrupts_states.sv"
`include "ipc.sv"


sequence reset_sequence;
  `rst ##1 !`rst;
endsequence


// Check for the reset behavior
property reset_p;
  reset_sequence |->
  FETCH_REQ() &&
  csrfile_mcause() == 0 &&
  csrfile_mepc() == 0 &&
  csrfile_mie() == 0 &&
  csrfile_mip() == 0 &&
  csrfile_misa() == 0 &&
  csrfile_mscratch() == 0 &&
  csrfile_mstatus() == 0 &&
  csrfile_mtval() == 0 &&
  csrfile_mtvec() == 0 &&
  excFlag() == 0 &&
  pcReg() == 0 &&
  phase() == fetch &&
  regfileWrite_dst() == 0 &&
  regfileWrite_dstData() == 0 &&
  toMemoryPort_sig_addrIn() == 0 &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  ecall_isa_Port_notify() == 0 &&
  fromMemoryPort_notify() == 0 &&
  isa_ecall_Port_notify() == 0 &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 0;
endproperty


property ECALL_DONE_to_FETCH_REQ_p;
  ECALL_DONE() &&
  ecall_isa_Port_sync()
|->
  ##1 (ecall_isa_Port_notify() == 0) and
  ##1 (fromMemoryPort_notify() == 0) and
  ##1 (isa_ecall_Port_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##1
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(pcReg(), 1), $past(excFlag(), 1)) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(pcReg(), 1), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(pcReg(), 1), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(pcReg(), 1), $past(excFlag(), 1)) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(pcReg(), 1), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(pcReg(), 1), $past(excFlag(), 1)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


property ECALL_REQ_to_ECALL_DONE_p;
  ECALL_REQ() &&
  isa_ecall_Port_sync()
|->
  ##1 (fromMemoryPort_notify() == 0) and
  ##1 (isa_ecall_Port_notify() == 0) and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##1
  ECALL_DONE() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  ecall_isa_Port_notify() == 1;
endproperty


// Checks if fetched ECALL type Instruction executed and update csrfile accordingly.
property FETCH_DONE_to_ECALL_REQ_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_ECALL)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0) and
  ##1 (toMemoryPort_notify() == 0)[*2] and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  ECALL_REQ() &&
  csrfile_mcause() == 11 &&
  csrfile_mepc() == (4 + $past(pcReg(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 2) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 1 &&
  isa_ecall_Port_sig() == 1 &&
  pcReg() == $past(pcReg(), 2) &&
  phase() == execute &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  isa_ecall_Port_notify() == 1;
endproperty


// Checks if fetched R-type instruction executed and written back to register file correctly.
property FETCH_DONE_to_FETCH_REQ_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_R)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


// Checks if fetched B-type instruction target address is aligned to four byte boundary and update csrfile  acoordingly.
property FETCH_DONE_to_FETCH_REQ_1_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  ((getBranchresult(fromMemoryPort_sig_loadedData(), getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 20) & 31)]), pcReg()) % 4) != 0)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mtval() == getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


// Checks if fetched JALR instruction Jump address is aligned to four byte boundary and update csrfile acoordingly.
property FETCH_DONE_to_FETCH_REQ_10_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_L) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_J) &&
  ((((`fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)] + getImmediate(fromMemoryPort_sig_loadedData())) & 4294967294) % 4) != 0)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mtval() == ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


// Checks if fetched JALR instruction Jump address is aligned to four byte boundary and update PC value and register file accordingly.
property FETCH_DONE_to_FETCH_REQ_11_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_L) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_J) &&
  !((((`fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)] + getImmediate(fromMemoryPort_sig_loadedData())) & 4294967294) % 4) != 0)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == (4 + $past(pcReg(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ((fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)] + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) & 4294967294), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == (4 + $past(pcReg(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


// Checks if fetched FENCE/FENCE.I instruction considered NOP.
property FETCH_DONE_to_FETCH_REQ_12_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_M)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_13_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_L) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_M) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 768) &&
  (((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getCSR(csrfile_mcause(), csrfile_mepc(), csrfile_mie(), mip_isa_Port_sig(), csrfile_misa(), csrfile_mscratch(), csrfile_mstatus(), csrfile_mtval(), csrfile_mtvec(), fromMemoryPort_sig_loadedData())) >> 11) & 3) == 3)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_14_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_L) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_M) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 768) &&
  !(((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getCSR(csrfile_mcause(), csrfile_mepc(), csrfile_mie(), mip_isa_Port_sig(), csrfile_misa(), csrfile_mscratch(), csrfile_mstatus(), csrfile_mtval(), csrfile_mtvec(), fromMemoryPort_sig_loadedData())) >> 11) & 3) == 3)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_15_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 772)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_16_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 773)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_17_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 833)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_18_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 834)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_19_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 832)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


// Checks if fetched B-type instruction target address is aligned to four byte boundary and update PC value accordingly.
property FETCH_DONE_to_FETCH_REQ_2_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !((getBranchresult(fromMemoryPort_sig_loadedData(), getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 20) & 31)]), pcReg()) % 4) != 0)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), getBranchresult($past(fromMemoryPort_sig_loadedData(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)]), $past(pcReg(), 2)), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_20_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 769)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == 1073741952 &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_21_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 835)
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_22_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC)) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 768) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 772) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 773) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 833) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 834) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 832) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 769) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 835)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_23_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_L) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_M) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 768) &&
  (((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), ((fromMemoryPort_sig_loadedData() >> 15) & 31), getCSR(csrfile_mcause(), csrfile_mepc(), csrfile_mie(), mip_isa_Port_sig(), csrfile_misa(), csrfile_mscratch(), csrfile_mstatus(), csrfile_mtval(), csrfile_mtvec(), fromMemoryPort_sig_loadedData())) >> 11) & 3) == 3)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_24_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_L) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_M) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 768) &&
  !(((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), ((fromMemoryPort_sig_loadedData() >> 15) & 31), getCSR(csrfile_mcause(), csrfile_mepc(), csrfile_mie(), mip_isa_Port_sig(), csrfile_misa(), csrfile_mscratch(), csrfile_mstatus(), csrfile_mtval(), csrfile_mtvec(), fromMemoryPort_sig_loadedData())) >> 11) & 3) == 3)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_25_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 772)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_26_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 773)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_27_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 833)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), (getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) & 4294967292), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_28_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 834)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE(getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_29_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 832)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


// Checks if fetched Store type Instruction effective address is naturally aligned for each data type and update csrfile accordingly.
property FETCH_DONE_to_FETCH_REQ_3_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SH) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData())) % 2) != 0)) || ((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SW) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData())) % 4) != 0)))
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(6, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(6, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(6, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(6, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mtval() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(6, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(6, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(6, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(6, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_30_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 769)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == 1073741952 &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), 1073741952, $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_31_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI)) &&
  (getImmediate(fromMemoryPort_sig_loadedData()) == 835)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), (($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31), getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_32_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI)) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 768) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 772) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 773) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 833) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 834) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 832) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 769) &&
  !(getImmediate(fromMemoryPort_sig_loadedData()) == 835)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getCSR($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(fromMemoryPort_sig_loadedData(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


// Checks if fetched EBREAK type Instruction executed and update csrfile accordingly.
property FETCH_DONE_to_FETCH_REQ_33_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_EBREAK)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(3, ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(3, ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1), ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(3, ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(3, ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1), ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(3, ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(3, ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1), ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(3, ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(3, ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), 1), ($past(pcReg(), 2) + 4), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


// Checks if fetched MRET type Instruction executed and update PC value and csrfile accordingly.
property FETCH_DONE_to_FETCH_REQ_34_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_MRET)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(csrfile_mepc(), 2), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(csrfile_mepc(), 2), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(csrfile_mepc(), 2), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(csrfile_mepc(), 2), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(csrfile_mepc(), 2), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), ($past(csrfile_mstatus(), 2) | 8), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(csrfile_mepc(), 2), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_35_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_S) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRW) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRS) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRC) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRWI) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRSI) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_CSRRCI) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_ECALL) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_EBREAK) &&
  !(getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_MRET)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


property FETCH_DONE_to_FETCH_REQ_36_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_ERR)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mtval() == 0 &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


// Checks if fetched U-type instruction executed and written back to register file correctly.
property FETCH_DONE_to_FETCH_REQ_4_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_U)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getALUresult_U($past(fromMemoryPort_sig_loadedData(), 2), $past(pcReg(), 2), getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getALUresult_U($past(fromMemoryPort_sig_loadedData(), 2), $past(pcReg(), 2), getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


// Checks if fetched J-type instruction Jump address is aligned to four byte boundary and update csrfile acoordingly.
property FETCH_DONE_to_FETCH_REQ_5_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  (((pcReg() + getImmediate(fromMemoryPort_sig_loadedData())) % 4) != 0)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mtval() == ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(0, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


// Checks if fetched J-type instruction Jump address is aligned to four byte boundary and update PC value and register file accordingly.
property FETCH_DONE_to_FETCH_REQ_6_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(((pcReg() + getImmediate(fromMemoryPort_sig_loadedData())) % 4) != 0)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == (4 + $past(pcReg(), 2)) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == (4 + $past(pcReg(), 2)) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


// Checks if fetched I_I-type instruction raises an illegal instruction exception and update csrfile acoordingly.
property FETCH_DONE_to_FETCH_REQ_7_p;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  ((((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SLLI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SRLI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SRAI)) && ((fromMemoryPort_sig_loadedData() & 33554432) != 0))
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mtval() == 0 &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(2, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), 0, $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


// Checks if fetched I_I-type instruction executed and written back to register file correctly.
property FETCH_DONE_to_FETCH_REQ_8_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  !((((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SLLI) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SRLI)) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SRAI)) && ((fromMemoryPort_sig_loadedData() & 33554432) != 0))
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  phase() == fetch &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 2), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), $past(excFlag(), 2)), $past(csrfile_mepc(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), $past(csrfile_mtval(), 2), $past(csrfile_mtvec(), 2), ($past(pcReg(), 2) + 4), $past(excFlag(), 2)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  toRegsPort_sig_dstData() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


// Checks if fetched Load type Instruction effective address is naturally aligned for each data type and update csrfile accordingly.
property FETCH_DONE_to_FETCH_REQ_9_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_L) &&
  ((((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_LH) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_LHU)) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData())) % 2) != 0)) || ((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_LW) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData())) % 4) != 0)))
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE(4, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE(4, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE(4, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(4, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1) &&
  csrfile_mtval() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE(4, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(4, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE(4, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), updateMEPC(updateMCAUSE(4, $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), 1), $past(pcReg(), 2), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1), $past(csrfile_mie(), 2), $past(mip_isa_Port_sig(), 2), $past(csrfile_misa(), 2), $past(csrfile_mscratch(), 2), $past(csrfile_mstatus(), 2), getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))), $past(csrfile_mtvec(), 2), $past(pcReg(), 2), 1) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


// Checks if fetched Load type Instruction access correct Memory location.
property FETCH_DONE_to_LOAD_REQ_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_U) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_J) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_I) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_I_L) &&
  !((((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_LH) || (getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_LHU)) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData())) % 2) != 0)) || ((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_LW) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData())) % 4) != 0)))
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  LOAD_REQ() &&
  csrfile_mcause() == $past(csrfile_mcause(), 2) &&
  csrfile_mepc() == $past(csrfile_mepc(), 2) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 2) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == $past(excFlag(), 2) &&
  pcReg() == $past(pcReg(), 2) &&
  phase() == execute &&
  regfileWrite_dst() == (($past(fromMemoryPort_sig_loadedData(), 2) >> 7) & 31) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == getMemoryMask(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))) &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


// Checks if fetched Store type Instruction access correct Memory location.
property FETCH_DONE_to_STORE_p;
    a_u_32_32 fromRegsPort_sig_at_t;
  FETCH_DONE() &&
  fromMemoryPort_sync() &&
  !(phase() == execute) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_R) &&
  !(getEncType(fromMemoryPort_sig_loadedData()) == ENC_B) &&
  (getEncType(fromMemoryPort_sig_loadedData()) == ENC_S) &&
  !(((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SH) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData())) % 2) != 0)) || ((getInstrType(fromMemoryPort_sig_loadedData()) == INSTR_SW) && ((getALUresult(getALUfunc(getInstrType(fromMemoryPort_sig_loadedData())), `fromRegsPort_sig[((fromMemoryPort_sig_loadedData() >> 15) & 31)], getImmediate(fromMemoryPort_sig_loadedData())) % 4) != 0)))
  ##0 hold(fromRegsPort_sig_at_t, `fromRegsPort_sig)
|->
  ##1 (ecall_isa_Port_notify() == 0)[*2] and
  ##1 (fromMemoryPort_notify() == 0)[*2] and
  ##1 (isa_ecall_Port_notify() == 0)[*2] and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0)[*2] and
  ##2
  STORE() &&
  csrfile_mcause() == $past(csrfile_mcause(), 2) &&
  csrfile_mepc() == $past(csrfile_mepc(), 2) &&
  csrfile_mie() == $past(csrfile_mie(), 2) &&
  csrfile_mip() == $past(mip_isa_Port_sig(), 2) &&
  csrfile_misa() == $past(csrfile_misa(), 2) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 2) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 2) &&
  csrfile_mtval() == $past(csrfile_mtval(), 2) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 2) &&
  excFlag() == $past(excFlag(), 2) &&
  pcReg() == $past(pcReg(), 2) &&
  phase() == execute &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 2) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 2) &&
  toMemoryPort_sig_addrIn() == getALUresult(getALUfunc(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))), fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 15) & 31)], getImmediate($past(fromMemoryPort_sig_loadedData(), 2))) &&
  toMemoryPort_sig_dataIn() == fromRegsPort_sig_at_t[(($past(fromMemoryPort_sig_loadedData(), 2) >> 20) & 31)] &&
  toMemoryPort_sig_mask() == getMemoryMask(getInstrType($past(fromMemoryPort_sig_loadedData(), 2))) &&
  toMemoryPort_sig_req() == ME_WR &&
  toMemoryPort_notify() == 1;
endproperty


property FETCH_REQ_to_FETCH_DONE_p;
  FETCH_REQ() &&
  toMemoryPort_sync()
|->
  ##1 (ecall_isa_Port_notify() == 0) and
  ##1 (isa_ecall_Port_notify() == 0) and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##1
  FETCH_DONE() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  fromMemoryPort_notify() == 1;
endproperty


property LOAD_DONE_to_FETCH_REQ_p;
  LOAD_DONE() &&
  fromMemoryPort_sync()
|->
  ##1 (ecall_isa_Port_notify() == 0) and
  ##1 (fromMemoryPort_notify() == 0) and
  ##1 (isa_ecall_Port_notify() == 0) and
  ##1
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(fromMemoryPort_sig_loadedData(), 1) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toRegsPort_sig_dst() == $past(regfileWrite_dst(), 1) &&
  toRegsPort_sig_dstData() == $past(fromMemoryPort_sig_loadedData(), 1) &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 1;
endproperty


property LOAD_REQ_to_LOAD_DONE_p;
  LOAD_REQ() &&
  toMemoryPort_sync()
|->
  ##1 (ecall_isa_Port_notify() == 0) and
  ##1 (isa_ecall_Port_notify() == 0) and
  ##1 (toMemoryPort_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##1
  LOAD_DONE() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  fromMemoryPort_notify() == 1;
endproperty


property STORE_to_FETCH_REQ_p;
  STORE() &&
  toMemoryPort_sync()
|->
  ##1 (ecall_isa_Port_notify() == 0) and
  ##1 (fromMemoryPort_notify() == 0) and
  ##1 (isa_ecall_Port_notify() == 0) and
  ##1 (toRegsPort_notify() == 0) and
  ##1
  FETCH_REQ() &&
  csrfile_mcause() == updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)) &&
  csrfile_mepc() == updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == updateMSTATUS(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == 0 &&
  pcReg() == updatePC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)) &&
  phase() == fetch &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  toMemoryPort_sig_addrIn() == updatePC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), updateMEPC(updateMCAUSE($past(csrfile_mcause(), 1), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), $past(excFlag(), 1)), $past(csrfile_mepc(), 1), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)), $past(csrfile_mie(), 1), $past(csrfile_mip(), 1), $past(csrfile_misa(), 1), $past(csrfile_mscratch(), 1), $past(csrfile_mstatus(), 1), $past(csrfile_mtval(), 1), $past(csrfile_mtvec(), 1), ($past(pcReg(), 1) + 4), $past(excFlag(), 1)) &&
  toMemoryPort_sig_dataIn() == 0 &&
  toMemoryPort_sig_mask() == MT_W &&
  toMemoryPort_sig_req() == ME_RD &&
  toMemoryPort_notify() == 1;
endproperty


property ECALL_DONE_wait_p;
  ECALL_DONE() &&
  !ecall_isa_Port_sync()
|->
  ##1
  ECALL_DONE() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  ecall_isa_Port_notify() == 1 &&
  fromMemoryPort_notify() == 0 &&
  isa_ecall_Port_notify() == 0 &&
  toMemoryPort_notify() == 0 &&
  toRegsPort_notify() == 0;
endproperty


property ECALL_REQ_wait_p;
  ECALL_REQ() &&
  !isa_ecall_Port_sync()
|->
  ##1
  ECALL_REQ() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  isa_ecall_Port_sig() == $past(isa_ecall_Port_sig(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  ecall_isa_Port_notify() == 0 &&
  fromMemoryPort_notify() == 0 &&
  isa_ecall_Port_notify() == 1 &&
  toMemoryPort_notify() == 0 &&
  toRegsPort_notify() == 0;
endproperty


property FETCH_DONE_wait_p;
  FETCH_DONE() &&
  !fromMemoryPort_sync()
|->
  ##1
  FETCH_DONE() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  ecall_isa_Port_notify() == 0 &&
  fromMemoryPort_notify() == 1 &&
  isa_ecall_Port_notify() == 0 &&
  toMemoryPort_notify() == 0 &&
  toRegsPort_notify() == 0;
endproperty


property FETCH_REQ_wait_p;
  FETCH_REQ() &&
  !toMemoryPort_sync()
|->
  ##1
  FETCH_REQ() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  toMemoryPort_sig_addrIn() == $past(toMemoryPort_sig_addrIn(), 1) &&
  toMemoryPort_sig_dataIn() == $past(toMemoryPort_sig_dataIn(), 1) &&
  toMemoryPort_sig_mask() == $past(toMemoryPort_sig_mask(), 1) &&
  toMemoryPort_sig_req() == $past(toMemoryPort_sig_req(), 1) &&
  ecall_isa_Port_notify() == 0 &&
  fromMemoryPort_notify() == 0 &&
  isa_ecall_Port_notify() == 0 &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 0;
endproperty


property LOAD_DONE_wait_p;
  LOAD_DONE() &&
  !fromMemoryPort_sync()
|->
  ##1
  LOAD_DONE() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  ecall_isa_Port_notify() == 0 &&
  fromMemoryPort_notify() == 1 &&
  isa_ecall_Port_notify() == 0 &&
  toMemoryPort_notify() == 0 &&
  toRegsPort_notify() == 0;
endproperty


property LOAD_REQ_wait_p;
  LOAD_REQ() &&
  !toMemoryPort_sync()
|->
  ##1
  LOAD_REQ() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  toMemoryPort_sig_addrIn() == $past(toMemoryPort_sig_addrIn(), 1) &&
  toMemoryPort_sig_dataIn() == $past(toMemoryPort_sig_dataIn(), 1) &&
  toMemoryPort_sig_mask() == $past(toMemoryPort_sig_mask(), 1) &&
  toMemoryPort_sig_req() == $past(toMemoryPort_sig_req(), 1) &&
  ecall_isa_Port_notify() == 0 &&
  fromMemoryPort_notify() == 0 &&
  isa_ecall_Port_notify() == 0 &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 0;
endproperty


property STORE_wait_p;
  STORE() &&
  !toMemoryPort_sync()
|->
  ##1
  STORE() &&
  csrfile_mcause() == $past(csrfile_mcause(), 1) &&
  csrfile_mepc() == $past(csrfile_mepc(), 1) &&
  csrfile_mie() == $past(csrfile_mie(), 1) &&
  csrfile_mip() == $past(csrfile_mip(), 1) &&
  csrfile_misa() == $past(csrfile_misa(), 1) &&
  csrfile_mscratch() == $past(csrfile_mscratch(), 1) &&
  csrfile_mstatus() == $past(csrfile_mstatus(), 1) &&
  csrfile_mtval() == $past(csrfile_mtval(), 1) &&
  csrfile_mtvec() == $past(csrfile_mtvec(), 1) &&
  excFlag() == $past(excFlag(), 1) &&
  pcReg() == $past(pcReg(), 1) &&
  phase() == $past(phase(), 1) &&
  regfileWrite_dst() == $past(regfileWrite_dst(), 1) &&
  regfileWrite_dstData() == $past(regfileWrite_dstData(), 1) &&
  toMemoryPort_sig_addrIn() == $past(toMemoryPort_sig_addrIn(), 1) &&
  toMemoryPort_sig_dataIn() == $past(toMemoryPort_sig_dataIn(), 1) &&
  toMemoryPort_sig_mask() == $past(toMemoryPort_sig_mask(), 1) &&
  toMemoryPort_sig_req() == $past(toMemoryPort_sig_req(), 1) &&
  ecall_isa_Port_notify() == 0 &&
  fromMemoryPort_notify() == 0 &&
  isa_ecall_Port_notify() == 0 &&
  toMemoryPort_notify() == 1 &&
  toRegsPort_notify() == 0;
endproperty
