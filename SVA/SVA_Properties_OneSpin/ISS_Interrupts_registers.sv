// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:51
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


// Visible registers


function bit[31:0] csrfile_mcause;
  csrfile_mcause = ISS_Interrupts.csrfile.mcause;
endfunction

function bit[31:0] csrfile_mepc;
  csrfile_mepc = ISS_Interrupts.csrfile.mepc;
endfunction

function bit[31:0] csrfile_mie;
  csrfile_mie = ISS_Interrupts.csrfile.mie;
endfunction

function bit[31:0] csrfile_mip;
  csrfile_mip = ISS_Interrupts.csrfile.mip;
endfunction

function bit[31:0] csrfile_misa;
  csrfile_misa = ISS_Interrupts.csrfile.misa;
endfunction

function bit[31:0] csrfile_mscratch;
  csrfile_mscratch = ISS_Interrupts.csrfile.mscratch;
endfunction

function bit[31:0] csrfile_mstatus;
  csrfile_mstatus = ISS_Interrupts.csrfile.mstatus;
endfunction

function bit[31:0] csrfile_mtval;
  csrfile_mtval = ISS_Interrupts.csrfile.mtval;
endfunction

function bit[31:0] csrfile_mtvec;
  csrfile_mtvec = ISS_Interrupts.csrfile.mtvec;
endfunction

function bit excFlag;
  excFlag = ISS_Interrupts.excFlag;
endfunction

function bit[31:0] pcReg;
  pcReg = ISS_Interrupts.pcReg_signal;
endfunction

function e_Phases phase;
  phase = ISS_Interrupts.phase;
endfunction

function bit[31:0] regfileWrite_dst;
  regfileWrite_dst = ISS_Interrupts.toRegsPort.dst;
endfunction

function bit[31:0] regfileWrite_dstData;
  regfileWrite_dstData = ISS_Interrupts.toRegsPort.dstData;
endfunction
