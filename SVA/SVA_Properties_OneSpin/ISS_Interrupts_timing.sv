// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:51
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


// Clock and reset


// Assign the model correct name of the reset signal used in it
`define rst ISS_Interrupts.rst
// Assign the model correct name of the clock signal used in it
`define clk ISS_Interrupts.clk
