// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:51
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------

// States


function bit FETCH_REQ;
  FETCH_REQ = ISS_Interrupts.phase == fetch && ISS_Interrupts.toMemoryPort_notify == 1'b1;
endfunction

function bit FETCH_DONE;
  FETCH_DONE = ISS_Interrupts.phase == fetch && ISS_Interrupts.fromMemoryPort_notify == 1'b1;
endfunction

function bit STORE;
  STORE = ISS_Interrupts.phase == execute && ISS_Interrupts.toMemoryPort_notify == 1'b1 && ISS_Interrupts.opcode  == ENC_S;
endfunction

function bit LOAD_REQ;
  LOAD_REQ = ISS_Interrupts.phase == execute && ISS_Interrupts.toMemoryPort_notify == 1'b1 && ISS_Interrupts.opcode  == ENC_I_L;
endfunction

function bit LOAD_DONE;
  LOAD_DONE = ISS_Interrupts.phase == execute && ISS_Interrupts.fromMemoryPort_notify == 1'b1 && ISS_Interrupts.opcode  == ENC_I_L;
endfunction

function bit ECALL_REQ;
  ECALL_REQ = ISS_Interrupts.phase == execute && ISS_Interrupts.isa_ecall_Port_notify == 1'b1
;
endfunction

function bit ECALL_DONE;
  ECALL_DONE = ISS_Interrupts.phase == execute && ISS_Interrupts.ecall_isa_Port_notify == 1'b1;
endfunction
