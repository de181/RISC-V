// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:51
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------

// Module binding


`include "ISS_Interrupts_assertions.sv"


bind ISS_Interrupts ISS_Interrupts_verification inst(.*, .reset(`rst));
