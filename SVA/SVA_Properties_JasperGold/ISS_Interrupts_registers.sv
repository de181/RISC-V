// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:49
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


// Visible registers


`define csrfile_mcause ISS_Interrupts.csrfile.mcause

`define csrfile_mepc ISS_Interrupts.csrfile.mepc

`define csrfile_mie ISS_Interrupts.csrfile.mie

`define csrfile_mip ISS_Interrupts.csrfile.mip

`define csrfile_misa ISS_Interrupts.csrfile.misa

`define csrfile_mscratch ISS_Interrupts.csrfile.mscratch

`define csrfile_mstatus ISS_Interrupts.csrfile.mstatus

`define csrfile_mtval ISS_Interrupts.csrfile.mtval

`define csrfile_mtvec ISS_Interrupts.csrfile.mtvec

`define excFlag ISS_Interrupts.excFlag

`define pcReg ISS_Interrupts.pcReg_signal

`define phase ISS_Interrupts.phase

`define regfileWrite_dst ISS_Interrupts.toRegsPort.dst

`define regfileWrite_dstData ISS_Interrupts.toRegsPort.dstData
