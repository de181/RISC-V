// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:49
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


// States


`define FETCH_REQ ISS_Interrupts.phase == fetch && ISS_Interrupts.toMemoryPort_notify == 1'b1

`define FETCH_DONE ISS_Interrupts.phase == fetch && ISS_Interrupts.fromMemoryPort_notify == 1'b1

`define STORE ISS_Interrupts.phase == execute && ISS_Interrupts.toMemoryPort_notify == 1'b1 && ISS_Interrupts.opcode  == ENC_S

`define LOAD_REQ ISS_Interrupts.phase == execute && ISS_Interrupts.toMemoryPort_notify == 1'b1 && ISS_Interrupts.opcode  == ENC_I_L

`define LOAD_DONE ISS_Interrupts.phase == execute && ISS_Interrupts.fromMemoryPort_notify == 1'b1 && ISS_Interrupts.opcode  == ENC_I_L

`define ECALL_REQ ISS_Interrupts.phase == execute && ISS_Interrupts.isa_ecall_Port_notify == 1'b1


`define ECALL_DONE ISS_Interrupts.phase == execute && ISS_Interrupts.ecall_isa_Port_notify == 1'b1
