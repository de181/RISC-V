// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Created on 22.06.2022 at 16:49
// Contact: contact@lubis-eda.com
// Author: Tobias Ludwig, Michael Schwarz
// -------------------------------------------------


// Data signals


`define ecall_isa_Port_sig ISS_Interrupts.ecall_isa_Port

`define fromMemoryPort_sig_loadedData ISS_Interrupts.fromMemoryPort.loadedData

`define fromRegsPort_sig ISS_Interrupts.fromRegsPort

`define fromRegsPort_sig_0 0

`define fromRegsPort_sig_1 0

`define fromRegsPort_sig_2 0

`define fromRegsPort_sig_3 0

`define fromRegsPort_sig_4 0

`define fromRegsPort_sig_5 0

`define fromRegsPort_sig_6 0

`define fromRegsPort_sig_7 0

`define fromRegsPort_sig_8 0

`define fromRegsPort_sig_9 0

`define fromRegsPort_sig_10 0

`define fromRegsPort_sig_11 0

`define fromRegsPort_sig_12 0

`define fromRegsPort_sig_13 0

`define fromRegsPort_sig_14 0

`define fromRegsPort_sig_15 0

`define fromRegsPort_sig_16 0

`define fromRegsPort_sig_17 0

`define fromRegsPort_sig_18 0

`define fromRegsPort_sig_19 0

`define fromRegsPort_sig_20 0

`define fromRegsPort_sig_21 0

`define fromRegsPort_sig_22 0

`define fromRegsPort_sig_23 0

`define fromRegsPort_sig_24 0

`define fromRegsPort_sig_25 0

`define fromRegsPort_sig_26 0

`define fromRegsPort_sig_27 0

`define fromRegsPort_sig_28 0

`define fromRegsPort_sig_29 0

`define fromRegsPort_sig_30 0

`define fromRegsPort_sig_31 0

`define isa_ecall_Port_sig ISS_Interrupts.isa_ecall_Port

`define mip_isa_Port_sig ISS_Interrupts.mip_isa_Port

`define toMemoryPort_sig_addrIn ISS_Interrupts.toMemoryPort.addrIn

`define toMemoryPort_sig_dataIn ISS_Interrupts.toMemoryPort.dataIn

`define toMemoryPort_sig_mask ISS_Interrupts.toMemoryPort.mask

`define toMemoryPort_sig_req ISS_Interrupts.toMemoryPort.req

`define toRegsPort_sig_dst ISS_Interrupts.toRegsPort.dst

`define toRegsPort_sig_dstData ISS_Interrupts.toRegsPort.dstData
