// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// RTL implementation of a RISCV ISA
// Contact: contact@lubis-eda.com
// Author: Luis Rivas, Sandeep Ragipati
// -------------------------------------------------

import top_level_types::*;
import iss_interrupts_types::*;

module ISS_Interrupts (
	//clock and reset
	input logic clk,
	input logic rst,

	//Memory Interface
	input st_MEtoCU_IF fromMemoryPort,
	input logic fromMemoryPort_sync,
	output logic fromMemoryPort_notify,
	output st_CUtoME_IF toMemoryPort,
	input logic toMemoryPort_sync,
	output logic toMemoryPort_notify,

	//Register file Interface
	input a_u_32_32 fromRegsPort,
	output st_RegfileWriteType toRegsPort,
	output logic toRegsPort_notify,

	//ECallHandler Interface
	output logic isa_ecall_Port,
	input logic isa_ecall_Port_sync,
	output logic isa_ecall_Port_notify,
	input logic ecall_isa_Port,
	input logic ecall_isa_Port_sync,
	output logic ecall_isa_Port_notify,

	//MIP Interface
	input logic[31:0] mip_isa_Port
);

    //ISS_Interrupts Phases
    e_Phases             phase, next_phase;

    //Program Counter
    logic[31:0]        pcReg_signal;
    logic[31:0]        nextPC_signal;

    //OPCODE
    e_EncType            opcode;
    e_InstrType_Complete instr_type;
    logic[31:0]        encodedInstr;
    logic[31:0]        encodedInstr_nxt;

    //Data for Exception Handling
    bit                excFlag;

    //Decoder
    logic               wr_reg;
    logic               wr_csr;
    logic				wr_store;
    logic               wr_load;
    logic               wr_ecall;
    logic               wr_mtval;
    logic               shift_exc;
    logic               mret;
    logic               instr_unkn;

    //Interrupt Hanlder
    logic               interrupt;

    //Memory and Register file
    logic              toMemoryPort_notify_nxt;
    logic              fromMemoryPort_notify_nxt;
    logic              isa_ecall_Port_notify_nxt;
    logic              ecall_isa_Port_notify_nxt;
    logic              toRegsPort_notify_nxt;
    st_RegfileWriteType   toRegsPort_nxt;
    st_CUtoME_IF          toMemoryPort_nxt;
    logic              isa_ecall_Port_nxt;

    //Control Status Registers
    st_CSRfileType     csrfile;
    logic[31:0]        mcause;
    logic[31:0]        mcause_int;
    logic[31:0]        mtval;
    logic[31:0]        mepc_exc;
    logic[31:0]        mepc_out;
    logic[31:0]        mip;
    logic[31:0]        mstatus_out;
    logic[31:0]        mie;
    logic[31:0]        mtvec_out;

    logic[31:0]        rs1_value;
    logic[31:0]        rs2_value;
    logic[4:0]         rs1;
    logic[4:0]         rd;
    logic[31:0]        imm;
    logic[31:0]        csr_data;
    logic[31:0]        aluResult;
    logic              stall;
    logic              pc_freeze;
    logic              pc_freeze_nxt;
    logic              ls_finish;
    logic              ecall_finish;
    logic              ls_finish_nxt;
    logic              ecall_finish_nxt;

    always_ff @(posedge clk, posedge rst) begin
	if (rst) begin
		phase			        <= fetch;
		toRegsPort		        <= '{default:0};
		toMemoryPort.addrIn	    <= 0;
		toMemoryPort.dataIn	    <= 0;
		toMemoryPort.mask	    <= MT_W;
		toMemoryPort.req	    <= ME_RD;
		encodedInstr		    <= 32'h00000013;
		toMemoryPort_notify	    <= 1'b1;
		fromMemoryPort_notify	<= 1'b0;
		toRegsPort_notify 	    <= 1'b0;
		isa_ecall_Port_notify	<= 1'b0;
		ecall_isa_Port_notify	<= 1'b0;
		ls_finish			    <= 1'b1;
		ecall_finish		    <= 1'b1;
		pc_freeze		        <= 1'b0;
		isa_ecall_Port		    <= 1'b0;
        end
    else begin
		encodedInstr		<= encodedInstr_nxt;
		phase			    <= next_phase;
		toRegsPort_notify	<= toRegsPort_notify_nxt;
		toRegsPort.dstData	<= toRegsPort_nxt.dstData;
		toRegsPort.dst		<= toRegsPort_nxt.dst;
		toMemoryPort.addrIn	<= toMemoryPort_nxt.addrIn;
		toMemoryPort.dataIn	<= toMemoryPort_nxt.dataIn;
		toMemoryPort.mask	<= toMemoryPort_nxt.mask;
		toMemoryPort.req	<= toMemoryPort_nxt.req;
		toMemoryPort_notify	<= toMemoryPort_notify_nxt;
		fromMemoryPort_notify	<= fromMemoryPort_notify_nxt;
		isa_ecall_Port_notify	<= isa_ecall_Port_notify_nxt;
		ecall_isa_Port_notify	<= ecall_isa_Port_notify_nxt;
		isa_ecall_Port		<= isa_ecall_Port_nxt;
		ls_finish			<= ls_finish_nxt;
		pc_freeze		    <= pc_freeze_nxt;
		ecall_finish		<= ecall_finish_nxt;
        end
    end

    always @(*) begin
        next_phase			        = phase;
        fromMemoryPort_notify_nxt	= fromMemoryPort_notify;
        toMemoryPort_notify_nxt		= toMemoryPort_notify;
        toRegsPort_notify_nxt		= toRegsPort_notify;
        isa_ecall_Port_notify_nxt	= isa_ecall_Port_notify;
        ecall_isa_Port_notify_nxt	= ecall_isa_Port_notify;
        encodedInstr_nxt		    = encodedInstr;
        toRegsPort_notify_nxt		= 1'b0;
        toRegsPort_nxt.dstData		= toRegsPort.dstData;
        toRegsPort_nxt.dst		    = toRegsPort.dst;
        toMemoryPort_nxt.addrIn		= toMemoryPort.addrIn;
        toMemoryPort_nxt.dataIn		= toMemoryPort.dataIn;
        toMemoryPort_nxt.mask		= toMemoryPort.mask;
        toMemoryPort_nxt.req		= toMemoryPort.req;
        stall				        = 1'b1;
        pc_freeze_nxt			    = pc_freeze;
        ls_finish_nxt			    = ls_finish;
        ecall_finish_nxt		    = ecall_finish;
        isa_ecall_Port_nxt		    = isa_ecall_Port;

        case (phase)
            //Fetch Next Instruction
            fetch: begin
                toRegsPort_notify_nxt     = 1'b0;
                isa_ecall_Port_notify_nxt = 1'b0;
                ecall_isa_Port_notify_nxt = 1'b0;
				if (toMemoryPort_sync == 1'b1 && toMemoryPort_notify == 1'b1) begin
					toMemoryPort_notify_nxt   = 1'b0;
					fromMemoryPort_notify_nxt = 1'b1;
				end
				if(fromMemoryPort_sync == 1'b1 && fromMemoryPort_notify == 1'b1) begin
					fromMemoryPort_notify_nxt = 1'b0;
					toMemoryPort_notify_nxt   = 1'b0;
					mip                       = mip_isa_Port;
					next_phase                = execute;
					encodedInstr_nxt          = fromMemoryPort.loadedData;
				end
            end
            //Execution of decoded instruction
            execute: begin
                stall = 1'b0;
                pc_freeze_nxt = 1'b1;
				//Execution of Load and Store Operation
				if((wr_store || wr_load) && !excFlag) begin
					if (ls_finish) begin
						toMemoryPort_nxt.addrIn	= aluResult;
						toMemoryPort_nxt.mask	= get_memory_mask(get_instr_type(encodedInstr));
						toMemoryPort_notify_nxt	= 1'b1;
						ls_finish_nxt = 1'b0;
					end
					if(wr_store) begin
						toMemoryPort_nxt.req	= ME_WR;
						toMemoryPort_nxt.dataIn	= rs2_value;
					if (toMemoryPort_sync == 1'b1 && toMemoryPort_notify == 1'b1) begin
						ls_finish_nxt = 1'b1;
						next_phase = fetch;
					end
					end
					else begin
					toRegsPort_nxt.dst	    = rd;
					toMemoryPort_nxt.req	= ME_RD;
					toMemoryPort_nxt.dataIn	= 0;
					if (toMemoryPort_sync == 1'b1 && toMemoryPort_notify == 1'b1) begin
						toMemoryPort_notify_nxt   = 1'b0;
						fromMemoryPort_notify_nxt = 1'b1;
					end
					if (fromMemoryPort_sync == 1'b1 && fromMemoryPort_notify == 1'b1) begin
						ls_finish_nxt = 1'b1;
						fromMemoryPort_notify_nxt	= 1'b0;
						next_phase			        = fetch;
						toRegsPort_nxt.dst		    = rd;
						toRegsPort_nxt.dstData	    = fromMemoryPort.loadedData;
						toRegsPort_notify_nxt	    = 1'b1;
					end
					end
				end
				//Execution of instructions with regfile writeback
				else if (wr_reg && !excFlag) begin
					toRegsPort_notify_nxt	= wr_reg;
					toRegsPort_nxt.dst		= rd;
					if (wr_csr) toRegsPort_nxt.dstData	= csr_data;
					else toRegsPort_nxt.dstData	= aluResult;
					next_phase = fetch;
				end
				//Execution of ECALL instruction
				else if(wr_ecall) begin
					if (ecall_finish) begin
						isa_ecall_Port_nxt		    = 1'b1;
						isa_ecall_Port_notify_nxt	= 1'b1;
						ecall_finish_nxt		    = 1'b0;
					end
					if (isa_ecall_Port_sync == 1'b1 && isa_ecall_Port_notify == 1'b1) begin
						isa_ecall_Port_notify_nxt	= 1'b0;
						ecall_isa_Port_notify_nxt	= 1'b1;
					end
					if (ecall_isa_Port_sync == 1'b1 && ecall_isa_Port_notify == 1'b1) begin
						ecall_finish_nxt		    = 1'b1;
						isa_ecall_Port_notify_nxt	= 1'b0;
						ecall_isa_Port_notify_nxt	= 1'b0;
						next_phase = fetch;
					end
				end
				else next_phase = fetch;
            end
	default: next_phase = fetch;
        endcase
        //Memory assignment for next instruction fetch
        if(next_phase == fetch && phase != fetch) begin
            pc_freeze_nxt	= 1'b0;
            if (excFlag || interrupt) begin
                toMemoryPort_nxt.addrIn	= mtvec_out;
            end
            else begin
                toMemoryPort_nxt.addrIn	= nextPC_signal;
            end
            toMemoryPort_nxt.dataIn	= 0;
            toMemoryPort_nxt.mask	= MT_W;
            toMemoryPort_nxt.req	= ME_RD;
            toMemoryPort_notify_nxt	= 1'b1;
            fromMemoryPort_notify_nxt	= 1'b0;
        end
    end

    //Decodes the current instruction
    decoder dec (
	.encodedInstr(encodedInstr),
	.regfile	(fromRegsPort),
	.csrfile	(csrfile),
	.opcode		(opcode),
	.instr_type	(instr_type),
	.rs1_value	(rs1_value),
	.rs2_value	(rs2_value),
	.rs1		(rs1),
	.rd		    (rd),
	.imm		(imm),
	.wr_reg		(wr_reg),
	.wr_csr		(wr_csr),
	.wr_store	(wr_store),
	.wr_load	(wr_load),
	.wr_ecall	(wr_ecall),
	.shift_exc	(shift_exc),
	.csr_data	(csr_data),
	.mret		(mret),
	.instr_unkn	(instr_unkn)
	);

    //Performs ALU operations
    alu alu_unit(
	.rs1_value	(rs1_value),
	.rs2_value	(rs2_value),
	.rs1		(rs1),
	.imm		(imm),
	.pcReg		(pcReg_signal),
	.opcode		(opcode),
	.instr_type	(instr_type),
	.csr_data	(csr_data),
	.aluResult	(aluResult)
	);

    //Performs exception handling
    exceptionHandler exc_handler(
	.instr_type	(instr_type),
	.pcReg		(pcReg_signal),
	.nextPC		(nextPC_signal),
	.aluResult	(aluResult),
	.excFlag	(excFlag),
	.wr_mtval	(wr_mtval),
	.mcause		(mcause),
	.mepc		(mepc_exc),
	.shift_exc	(shift_exc),
	.stall		(stall),
	.instr_unkn	(instr_unkn),
	.mtval		(mtval)
    );

    //Performs CSRFile updates
    csrfile_reg csrs(
        .clk		(clk),
        .rst		(rst),
        .stall		(stall),
        .wr_data	(aluResult),
        .csr_addr	(imm),
        .mcause_exc	(mcause),
        .mcause_int	(mcause_int),
        .mip		(mip),
        .mtval_exc	(mtval),
        .mepc_exc	(mepc_exc),
        .wr_csr		(wr_csr),
        .excFlag	(excFlag),
        .interrupt	(interrupt),
        .wr_mtval	(wr_mtval),
        .csrfile	(csrfile),
        .mstatus_out(mstatus_out),
        .mie_int	(mie),
        .mtvec_out	(mtvec_out),
        .nextPC		(nextPC_signal),
        .mret		(mret),
        .mepc_out	(mepc_out),
        .ecall_finish(ecall_finish_nxt)
    );

    //Performs PC Calculations and assigns PC accordingly
	pc pc_calc(
	.clk		(clk),
	.rst		(rst),
	.mret		(mret),
	.instr_type	(instr_type),
	.opcode		(opcode),
	.imm		(imm),
	.excFlag	(excFlag),
	.interrupt	(interrupt),
	.mtvec_in	(mtvec_out),
	.aluResult	(aluResult),
	.rs1_value	(rs1_value),
	.pcReg		(pcReg_signal),
	.nextPC		(nextPC_signal),
	.pc_freeze	(pc_freeze_nxt),
	.stall		(stall),
	.wr_ecall	(wr_ecall),
	.mepc_in	(mepc_out)
	);

    //Performs Interrupt handling
    intHandler extInterrupts(
	.mip		(mip),
	.mie		(mie),
	.mstatus_in	(mstatus_out),
	.mcause_int	(mcause_int),
	.wr_csr		(wr_csr),
	.csr_addr	(imm),
	.csr_data	(aluResult),
	.mret		(mret),
	.stall		(stall),
	.pc_freeze	(pc_freeze_nxt),
	.interrupt	(interrupt)
	);
endmodule

//Decodes the current instruction to execute and writeback
module decoder (
	input  logic[31:0]		encodedInstr,
	input a_u_32_32		regfile,
   	st_CSRfileType			csrfile,
	output e_EncType			opcode,
	output e_InstrType_Complete	instr_type,
	output logic[31:0]		rs1_value,
	output logic[31:0]		rs2_value,
	output logic[4:0]		rs1,
	output logic[4:0]		rd,
	output logic[31:0]		imm,
	output logic			wr_reg,
	output logic			wr_csr,
	output logic			wr_store,
	output logic			wr_load,
	output logic			wr_ecall,
	output logic			shift_exc,
	output logic[31:0]		csr_data,
	output logic			instr_unkn,
	output logic			mret
);
	assign opcode		= get_enc_type(encodedInstr);
	assign instr_type	= get_instr_type(encodedInstr);
	assign rs1		= encodedInstr[19:15];
	assign rs1_value	= regfile[encodedInstr[19:15]]; //rs1 = encodedInstr[19:15]
	assign rs2_value	= regfile[encodedInstr[24:20]]; //rs2 = encodedInstr[24:20]
	assign rd		= encodedInstr[11:7];           //rd = encodedInstr[11:7]
	assign imm		= get_immediate(encodedInstr);
	assign csr_data		= get_CSR(csrfile, encodedInstr);

	always @(opcode, instr_type, encodedInstr) begin
		wr_reg      = 1'b0;
		wr_csr      = 1'b0;
		wr_store    = 1'b0;
		wr_load     = 1'b0;
		wr_ecall    = 1'b0;
		shift_exc   = 1'b0;
		mret        = 1'b0;
		instr_unkn  = 1'b0;
		//R-Type
		if ((opcode == ENC_R || opcode == ENC_I_I || opcode == ENC_I_J || opcode == ENC_J || opcode == ENC_U)) begin
			wr_reg = 1'b1;
			//Illegal instruction exception for ENC_I_I type
			if ((encodedInstr & SHAMT5) != 0) shift_exc = 1'b1;
		end
		//Load and Store
		if(opcode == ENC_S || opcode == ENC_I_L) begin
			if(opcode == ENC_S) wr_store = 1'b1;
			else wr_load =1'b1;
		end
		//I_S-Type
		if(opcode == ENC_I_S) begin
			if (instr_type == INSTR_CSRRC  || instr_type == INSTR_CSRRCI || instr_type == INSTR_CSRRS ||
			instr_type == INSTR_CSRRSI || instr_type == INSTR_CSRRW  || instr_type == INSTR_CSRRWI) begin
			wr_csr = 1'b1;
			wr_reg = 1'b1;
			end
		else if(instr_type == INSTR_ECALL) wr_ecall = 1'b1;
		else if (instr_type == INSTR_MRET) mret = 1'b1;
		end
		//Unknown type
		if (opcode == ENC_ERR) instr_unkn = 1'b1;

	end
endmodule

//Performs alu operations and stores aluresult
module alu (
	input logic[31:0]          rs1_value,
	input logic[31:0]          rs2_value,
	input logic[4:0]           rs1,
	input logic[31:0]          imm,
	input logic[31:0]          pcReg,
	input e_EncType            opcode,
	input e_InstrType_Complete instr_type,
	input logic[31:0]          csr_data,
	output logic[31:0]         aluResult
);

	logic[31:0] aluOp1;
	logic[31:0] aluOp2;

	always @(opcode, instr_type, imm, csr_data, rs1, rs1_value, rs2_value, pcReg) begin
		if (instr_type == INSTR_CSRRCI || instr_type == INSTR_CSRRSI || instr_type == INSTR_CSRRWI) begin
			aluOp1	= rs1;
		end
		else aluOp1 = rs1_value;

		if (opcode != ENC_R && opcode != ENC_B && opcode != ENC_I_S) begin
			aluOp2	= imm;
		end
		else if (opcode == ENC_I_S) begin
			aluOp2	= csr_data;
		end
		else begin
			aluOp2	= rs2_value;
		end
		if (opcode == ENC_I_J || opcode == ENC_J) begin
			aluResult	= pcReg + 4;
		end
		else if (opcode == ENC_U) begin
			aluResult	= get_ENC_U_ALU_result(imm, pcReg, instr_type);
		end
		else begin
			aluResult	= get_alu_result(get_alu_funct(instr_type), aluOp1, aluOp2);
		end
	end
endmodule

//Respective csr values assigned according to the exception triggered
module exceptionHandler(
	input e_InstrType_Complete instr_type,
	input logic[31:0]        pcReg,
	input logic[31:0]        nextPC,
	input logic[31:0]        aluResult,
	input logic              shift_exc,
	input logic              stall,
	input logic              instr_unkn,
	output logic             excFlag,
	output logic             wr_mtval,
	output logic[31:0]       mcause,
	output logic[31:0]       mepc,
	output logic[31:0]       mtval
);

	always_comb	begin
		wr_mtval = 1'b0;
		excFlag  = 1'b0;
		mcause   = 32'b0;
		mepc     = 32'b0;
		mtval    = 32'b0;

		if (!stall) begin
			if((instr_type == INSTR_SLLI || instr_type == INSTR_SRLI || instr_type == INSTR_SRAI) && shift_exc) begin
				mcause      = 2;
				mtval       = 0;
				mepc        = pcReg;
				wr_mtval    = 1'b1;
				excFlag     = 1'b1;
			end
			else if ((instr_type == INSTR_SH && aluResult % 2 != 0) ||
					 (instr_type == INSTR_SW && aluResult % 4 != 0)) begin
				mcause   = 6;
				mtval    = aluResult;
				mepc     = pcReg;
				wr_mtval = 1'b1;
				excFlag  = 1'b1;
			end
			else if (((instr_type == INSTR_LH || instr_type == INSTR_LHU) && aluResult % 2 != 0) ||
					  (instr_type == INSTR_LW && aluResult % 4 != 0)) begin
				mcause   = 4;
				mtval    = aluResult;
				mepc     = pcReg;
				wr_mtval = 1'b1;
				excFlag  = 1'b1;
			end
			if (instr_type == INSTR_ECALL) begin
				mcause  = 11;
				mepc    = pcReg + 4;
				excFlag = 1'b1;
			end
			else if (instr_type == INSTR_EBREAK) begin
				mcause  = 3;
				mepc    = pcReg + 4;
				excFlag = 1'b1;
			end
			else if (instr_unkn) begin
				mcause   = 2;
				mtval    = 0;
				mepc     = pcReg;
				wr_mtval = 1'b1;
				excFlag  = 1'b1;
			end
			else if (nextPC % 4 != 0) begin
				mcause   = 0;
				mtval    = nextPC;
				mepc     = pcReg;
				wr_mtval = 1'b1;
				excFlag  = 1'b1;
			end
		end
	end
endmodule

//CSR regfile updates
module csrfile_reg (
	input logic          clk,
	input logic          rst,
	input logic          stall,
	input logic[31:0]    wr_data,
	input logic[31:0]    csr_addr,
	input logic[31:0]    mip,
	input logic[31:0]    mcause_exc,
	input logic[31:0]    mcause_int,
	input logic[31:0]    mtval_exc,
	input logic[31:0]    mepc_exc,
	input logic[31:0]    nextPC,
	input logic          wr_csr,
	input logic          excFlag,
	input logic          wr_mtval,
	input logic          interrupt,
	input logic          mret,
	output logic[31:0]   mstatus_out,
	output logic[31:0]   mie_int,
	output logic[31:0]   mtvec_out,
	output logic[31:0]   mepc_out,
	input logic          ecall_finish,
	output st_CSRfileType   csrfile
);
	logic[31:0]    mcause_nxt;
	logic[31:0]    mie_nxt;
	logic[31:0]    mip_nxt;
	logic[31:0]    mtvec_nxt;
	logic[31:0]    mstatus_nxt;
	logic[31:0]    mepc_nxt;
	logic[31:0]    misa_nxt;
	logic[31:0]    mtval_nxt;
	logic[31:0]    mscratch_nxt;

	assign mstatus_out = csrfile.mstatus;
	assign mie_int     = mie_nxt;
	assign mtvec_out   = mtvec_nxt;
	assign mepc_out    = csrfile.mepc;

	always_comb begin
		mcause_nxt   = csrfile.mcause;
		mie_nxt      = csrfile.mie;
		mtvec_nxt    = csrfile.mtvec;
		mstatus_nxt  = csrfile.mstatus;
		mepc_nxt     = csrfile.mepc;
		misa_nxt     = csrfile.misa;
		mtval_nxt    = csrfile.mtval;
		mscratch_nxt = csrfile.mscratch;
		mip_nxt      = mip;

	if (wr_csr) begin
		case (csr_addr)
                    MCSR_MSTATUS: begin
                        if (wr_data[12:11] == 2'b11) begin
                            mstatus_nxt = wr_data;
                        end
                    end
                    MCSR_MIE:      mie_nxt      = wr_data;
                    MCSR_MTVEC:    mtvec_nxt    = {wr_data[31:2], 2'b00};
                    MCSR_MISA:     misa_nxt     = 32'h40000080;
                    MCSR_MSCRATCH: mscratch_nxt = wr_data;
                    MCSR_MEPC:     mepc_nxt     = {wr_data[31:2], 2'b00};
                    MCSR_MCAUSE:   mcause_nxt   = wr_data;
                    MCSR_MTVAL:    mtval_nxt    = wr_data;
                endcase
	end

	if (mret) begin
		mstatus_nxt = mstatus_nxt | unsigned'(32'h00000008);
	end


	if (excFlag) begin
		mcause_nxt  = mcause_exc;
		mepc_nxt    = mepc_exc;
		if(ecall_finish) mstatus_nxt = mstatus_nxt & unsigned'(32'hFFFFFFF7);
		if (wr_mtval) begin
		    mtval_nxt = mtval_exc;
		end
	end
	else if (interrupt) begin
		mcause_nxt  = mcause_int;
		mstatus_nxt = mstatus_nxt & unsigned'(32'hFFFFFFF7);
		mepc_nxt    = nextPC;
	end
	end

	always_ff @(posedge clk, posedge rst) begin
		if (rst) begin
			csrfile <= '{default:0};
		end
		else begin
			if (!stall) begin
				csrfile.mip      <= mip_nxt;
				csrfile.mstatus  <= mstatus_nxt;
				csrfile.mepc     <= mepc_nxt;
				csrfile.mcause   <= mcause_nxt;
				csrfile.mtval    <= mtval_nxt;
				csrfile.mie      <= mie_nxt;
				csrfile.misa     <= misa_nxt;
				csrfile.mscratch <= mscratch_nxt;
				csrfile.mtvec    <= mtvec_nxt;
			end
		end
	end
endmodule

//calculates PC based on the type of instruction
module pc (
	input logic		    clk,
	input logic		    rst,
	input logic		    mret,
	input e_EncType	    opcode,
	input logic[31:0]   imm,
	input logic		    excFlag,
	input logic		    interrupt,
	input logic[31:0]   mtvec_in,
	input logic[31:0]   mepc_in,
	input logic[31:0]   aluResult,
	input logic[31:0]   rs1_value,
	input e_InstrType_Complete	instr_type,
	input logic			pc_freeze,
	input logic			stall,
	input logic         wr_ecall,
	output logic[31:0]  pcReg,
	output logic[31:0]  nextPC
);

	always @(instr_type, opcode, aluResult, imm, pcReg, rs1_value, stall, pc_freeze, mepc_in, mtvec_in, mret, wr_ecall) begin
		nextPC = pcReg;

		if (!stall && !pc_freeze) begin
			if (opcode == ENC_B) nextPC = branch_PC_calculation(instr_type, aluResult, pcReg, imm);
			else if (opcode == ENC_I_J) nextPC = (rs1_value + imm) & 32'hFFFFFFFE;
			else if (opcode == ENC_J) nextPC = pcReg + imm ;
			else if (mret) nextPC = mepc_in;
			else if (wr_ecall) nextPC = mtvec_in;
			else nextPC = pcReg + 4;
		end
	end

	always_ff @(posedge clk, posedge rst) begin
		if (rst) begin
			pcReg <= 0;
		end
		else begin
			if ((excFlag || interrupt) && !stall && !wr_ecall) begin
				pcReg <= mtvec_in;
			end
			else begin
				pcReg <= nextPC;
			end
		end
	end
endmodule

//Checks for the interrupts during every fetch cycle
module intHandler (
	input logic[31:0]	mip,
	input logic[31:0]	mie,
	input logic[31:0]	mstatus_in,
	input logic		    stall,
	input logic		    pc_freeze,
	input logic		    wr_csr,
	input logic[31:0]	csr_addr,
	input logic[31:0]	csr_data,
	input logic		    mret,
	output logic[31:0]	mcause_int,
	output logic		interrupt
);
	logic [31:0] mstatus_tmp;

	always_comb begin
		mcause_int = 32'b0;
		interrupt = 0;
		mstatus_tmp = 0;

		if (wr_csr && csr_addr == MCSR_MSTATUS && csr_data[12:11] == 2'b11) begin
			mstatus_tmp = csr_data;
		end
		else if (mret) begin
			mstatus_tmp = mstatus_in | unsigned'(32'h00000008);
		end
		else begin
			mstatus_tmp = mstatus_in;
		end

		if (!stall && !pc_freeze) begin
			interrupt  =  MSTATUS_MIE(mstatus_tmp) &
						  (MTRAP_MEI(mie) & MTRAP_MEI(mip) |
						  MTRAP_MSI(mie)  & MTRAP_MSI(mip) |
						  MTRAP_MTI(mie)  & MTRAP_MTI(mip));

			if ((MTRAP_MEI(mie)) && (MTRAP_MEI(mip))) begin
				mcause_int  = 32'h8000000B;
			end
			else if ((MTRAP_MSI(mie)) && (MTRAP_MSI(mip))) begin
				mcause_int  = 32'h80000003;
			end
			else if ((MTRAP_MTI(mie)) && (MTRAP_MTI(mip))) begin
				mcause_int  = 32'h80000007;
			end
		end
    end
endmodule
