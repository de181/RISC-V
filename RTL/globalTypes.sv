// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// RTL implementation of a RISCV ISA
// Contact: contact@lubis-eda.com
// Author: Luis Rivas, Sandeep Ragipati
// -------------------------------------------------

package top_level_types;

// Global data types


// Enumeration types

typedef enum { ALU_ADD, ALU_AND, ALU_COPY1, ALU_CSRRW, ALU_CSRRC, ALU_OR, ALU_SLL, ALU_SLT, ALU_SLTU, ALU_SRA, ALU_SRL, ALU_SUB, ALU_X, ALU_XOR } e_ALUfuncType;

typedef enum { ENC_B, ENC_ERR, ENC_I_I, ENC_I_J, ENC_I_L, ENC_I_M, ENC_I_S, ENC_J, ENC_R, ENC_S, ENC_U } e_EncType;

typedef enum { INSTR_ADD, INSTR_ADDI, INSTR_AND, INSTR_ANDI, INSTR_AUIPC, INSTR_BEQ, INSTR_BGE, INSTR_BGEU, INSTR_BLT, INSTR_BLTU, INSTR_BNE, INSTR_CSRRC, INSTR_CSRRCI, INSTR_CSRRS, INSTR_CSRRSI, INSTR_CSRRW, INSTR_CSRRWI, INSTR_ECALL, INSTR_EBREAK, INSTR_FENCE, INSTR_FENCEI, INSTR_JAL, INSTR_JALR, INSTR_LB, INSTR_LBU, INSTR_LH, INSTR_LHU, INSTR_LUI, INSTR_LW, INSTR_MRET, INSTR_OR, INSTR_ORI, INSTR_SB, INSTR_SH, INSTR_SLL, INSTR_SLLI, INSTR_SLT, INSTR_SLTI, INSTR_SLTU, INSTR_SLTUI, INSTR_SRA, INSTR_SRAI, INSTR_SRL, INSTR_SRLI, INSTR_SUB, INSTR_SW, INSTR_UNKNOWN, INSTR_XOR, INSTR_XORI } e_InstrType_Complete;

typedef enum { ME_RD, ME_WR } e_ME_AccessType;

typedef enum { MT_B, MT_BU, MT_H, MT_HU, MT_W, MT_X } e_ME_MaskType;


// Compound types

typedef struct {
  bit[31:0] mcause;
  bit[31:0] mepc;
  bit[31:0] mie;
  bit[31:0] mip;
  bit[31:0] misa;
  bit[31:0] mscratch;
  bit[31:0] mstatus;
  bit[31:0] mtval;
  bit[31:0] mtvec;
} st_CSRfileType;

typedef struct {
  bit[31:0] addrIn;
  bit[31:0] dataIn;
  e_ME_MaskType mask;
  e_ME_AccessType req;
} st_CUtoME_IF;

typedef struct {
  bit[31:0] loadedData;
} st_MEtoCU_IF;

typedef struct {
  bit[31:0] dst;
  bit[31:0] dstData;
} st_RegfileWriteType;


// Array types

typedef bit[31:0] a_u_32_32 [31:0];

	parameter unsigned MEM_SIZE = 65536;
	const int MEM_START_ADDRESS = 32'h00000000;

	typedef enum logic [1:0] {
		mem_req,
		mem_store_done,
		mem_load_done
	} MemSections;

	typedef  logic[7:0] mem8_type [0:MEM_SIZE-1];
	typedef  logic[31:0] mem32_type [0:MEM_SIZE-1];

endpackage
