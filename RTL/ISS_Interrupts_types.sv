// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// RTL implementation of a RISCV ISA
// Contact: contact@lubis-eda.com
// Author: Luis Rivas, Sandeep Ragipati
// -------------------------------------------------

package iss_interrupts_types;

	import top_level_types::*;
typedef enum { execute, fetch } e_Phases;

	typedef enum logic [2:0]{
		idle,
		ecall_req,
		ecall_done,
		mem_req_ph,
		mem_done_ph
	} temp_Phases;

	const bit[6:0] OPCODE_R	  	= 8'h33;  //(OP:0110011)
	const bit[6:0] OPCODE_I_I 	= 8'h13;  //(OP_IMM:0010011)
	const bit[6:0] OPCODE_I_L 	= 8'h03;  //(LOAD:0000011)
	const bit[6:0] OPCODE_I_J 	= 8'h67;  //(JALR:1100111)
	const bit[6:0] OPCODE_S   	= 8'h23;  //(STORE:0100011)
	const bit[6:0] OPCODE_B   	= 8'h63;  //(BRANCH:1100011)
	const bit[6:0] OPCODE_U1  	= 8'h37;  //(LUI:0110111)
	const bit[6:0] OPCODE_U2  	= 8'h17;  //(AUIPC:0010111)
	const bit[6:0] OPCODE_J   	= 8'h6F;  //(JAL:1101111)
	const bit[6:0] OPCODE_I_M	= 8'h0F;  //(MISC_MEM:0001111)
	const bit[6:0] OPCODE_I_S	= 8'h73;  //(SYSTEM:1110011)

	const bit[31:0] MCSR_MSTATUS	= 32'h300;
	const bit[31:0] MCSR_MIE		= 32'h304;
	const bit[31:0] MCSR_MTVEC		= 32'h305;
	const bit[31:0] MCSR_MEPC		= 32'h341;
	const bit[31:0] MCSR_MCAUSE		= 32'h342;
	const bit[31:0] MCSR_MIP		= 32'h344;
	const bit[31:0] MCSR_MSCRATCH	= 32'h340;
	const bit[31:0] MCSR_MISA		= 32'h301;
	const bit[31:0] MCSR_MTVAL		= 32'h343;

	const bit[31:0] PRIVCODE_ECALL  = 32'h000;
	const bit[31:0] PRIVCODE_EBREAK = 32'h001;
	const bit[31:0] PRIVCODE_MRET   = 32'h302;

	const bit[31:0] SHAMT5 = 32'h02000000;
	const bit[31:0] JUMP_MASK = 32'hFFFFFFFE;

	function logic MSTATUS_MIE(logic[31:0] x);
		return x[3];
	endfunction: MSTATUS_MIE;

	function logic MTRAP_MEI(logic[31:0] x);
		return x[11];
	endfunction: MTRAP_MEI;

	function logic MTRAP_MSI(logic[31:0] x);
		return x[3];
	endfunction: MTRAP_MSI;

	function logic MTRAP_MTI (logic[31:0] x);
		return x[7];
	endfunction: MTRAP_MTI;

	function e_EncType get_enc_type(bit[31:0] encodedInstr);
		bit[6:0] opcode;
		opcode = encodedInstr[6:0];
		if (opcode == OPCODE_R) 	return ENC_R;
		else if (opcode == OPCODE_I_I)  return ENC_I_I;
		else if (opcode == OPCODE_I_L)  return ENC_I_L;
		else if (opcode == OPCODE_I_J)  return ENC_I_J;
		else if (opcode == OPCODE_S)    return ENC_S;
		else if (opcode == OPCODE_B)    return ENC_B;
		else if (opcode == OPCODE_U1 || opcode == OPCODE_U2)  return ENC_U;
		else if (opcode == OPCODE_J)    return ENC_J;
		else if (opcode == OPCODE_I_M)  return ENC_I_M;
		else if (opcode == OPCODE_I_S)  return ENC_I_S;
		else return ENC_ERR;
	endfunction: get_enc_type;

	function e_InstrType_Complete get_instr_type(bit[31:0] encodedInstr);
		bit[6:0] opcode;
		bit [2:0] funct3;
		bit [6:0] funct7;
		funct3 = encodedInstr[14:12];
        funct7 = encodedInstr[31:25];
		opcode = encodedInstr[6:0];
		if (opcode == OPCODE_R) begin
			if (funct3 == 'h00) begin
				if (funct7 == 'h00) begin
					return INSTR_ADD;
				end
				else if (funct7 =='h20) begin
					return INSTR_SUB;
				end
				else begin
					return INSTR_UNKNOWN;
				end
			end
			else if (funct3 == 'h01) begin
				return INSTR_SLL;
			end
			else if (funct3 == 'h02) begin
				return INSTR_SLT;
			end
			else if (funct3 == 'h03) begin
				return INSTR_SLTU;
			end
			else if (funct3 == 'h04) begin
				return INSTR_XOR;
			end
			else if (funct3 == 'h05) begin
				if (funct7 == 'h00) begin
					return INSTR_SRL;
				end
				else if (funct7 == 'h20) begin
					return INSTR_SRA;
				end
				else begin
					return INSTR_UNKNOWN;
				end
			end
			else if (funct3 == 'h06) begin
				return INSTR_OR;
			end
			else if (funct3 == 'h07) begin
				return INSTR_AND;
			end
			else begin
				return INSTR_UNKNOWN;
			end
		end
		else if (opcode == OPCODE_I_I) begin
			if (funct3 == 'h00) begin
				return INSTR_ADDI;
			end
			else if (funct3 == 'h01) begin
				return INSTR_SLLI;
			end
			else if (funct3 == 'h02) begin
				return INSTR_SLTI;
			end
			else if (funct3 == 'h03) begin
				return INSTR_SLTUI;
			end
			else if (funct3 == 'h04) begin
				return INSTR_XORI;
			end
			else if (funct3 == 'h05) begin
				if (funct7 == 'h00) begin
					return INSTR_SRLI;
				end
				else if (funct7 == 'h20) begin
					return INSTR_SRAI;
				end
				else begin
					return INSTR_UNKNOWN;
				end
			end
			else if (funct3 == 'h06) begin
				return INSTR_ORI;
			end
			else if (funct3 == 'h07) begin
				return INSTR_ANDI;
			end
			else begin
				return INSTR_UNKNOWN;
			end
		end
		else if (opcode == OPCODE_I_L) begin
			if (funct3 == 'h00) begin
				return INSTR_LB;
			end
			else if (funct3 == 'h01) begin
				return INSTR_LH;
			end
         		else if (funct3 == 'h02) begin
				return INSTR_LW;
			end
         		else if (funct3 == 'h04) begin
				return INSTR_LBU;
			end
        		else if (funct3 == 'h05) begin
				return INSTR_LHU;
			end
        		else begin
				return INSTR_UNKNOWN;
			end
		end
    	else if (opcode == OPCODE_I_J) begin
			return INSTR_JALR;
		end
		else if (opcode == OPCODE_I_M) begin
			if (funct3 == 'h00) begin
				return INSTR_FENCE;
			end
			else if (funct3 == 'h01) begin
				return INSTR_FENCEI;
			end
			else begin
				return INSTR_UNKNOWN;
			end
		end
		else if (opcode == OPCODE_I_S) begin
			if (funct3 == 'h00) begin
				if(PRIVCODE_ECALL == {20'h00000, encodedInstr[31:20]}) begin
			        return INSTR_ECALL;
		        end
		        else if(PRIVCODE_EBREAK == {20'h00000, encodedInstr[31:20]}) begin
			        return INSTR_EBREAK;
		        end
		        else if(PRIVCODE_MRET == {20'h00000, encodedInstr[31:20]}) begin
			        return INSTR_MRET;
		        end
		        else return INSTR_UNKNOWN;
			end
			else if (funct3 == 'h01) begin
				return INSTR_CSRRW;
			end
         		else if (funct3 == 'h02) begin
				return INSTR_CSRRS;
			end
         		else if (funct3 == 'h03) begin
				return INSTR_CSRRC;
			end
        		else if (funct3 == 'h05) begin
				return INSTR_CSRRWI;
			end
			else if (funct3 == 'h06) begin
				return INSTR_CSRRSI;
			end
			else if (funct3 == 'h07) begin
				return INSTR_CSRRCI;
			end
        	else begin
				return INSTR_UNKNOWN;
			end
		end
    		else if (opcode == OPCODE_S) begin
			if (funct3 == 'h00) begin
				return INSTR_SB;
			end
         		else if (funct3 == 'h01) begin
				return INSTR_SH;
			end
         		else if (funct3 == 'h02) begin
				return INSTR_SW;
			end
         		else begin
				return INSTR_UNKNOWN;
			end
		end
     		else if (opcode == OPCODE_B) begin
			if (funct3 == 'h00) begin
				return INSTR_BEQ;
			end
        		else if (funct3 == 'h01) begin
				return INSTR_BNE;
			end
        		else if (funct3 == 'h04) begin
				return INSTR_BLT;
			end
         		else if (funct3 == 'h05) begin
				return INSTR_BGE;
			end
        		else if (funct3 == 'h06) begin
				return INSTR_BLTU;
			end
         		else if (funct3 == 'h07) begin
				return INSTR_BGEU;
			end
        		else begin
				return INSTR_UNKNOWN;
			end
		end
     		else if (opcode == OPCODE_U1) begin
			return INSTR_LUI;
		end
     		else if (opcode == OPCODE_U2) begin
			return INSTR_AUIPC;
		end
     		else if (opcode == OPCODE_J) begin
			return INSTR_JAL;
		end
     		else begin
			return INSTR_UNKNOWN;
		end
	endfunction: get_instr_type;

	function e_ALUfuncType get_alu_funct(e_InstrType_Complete instr);
		if(instr == INSTR_ADD || instr == INSTR_ADDI || instr == INSTR_LB || instr == INSTR_LH || instr == INSTR_LW || instr == INSTR_LBU || instr == INSTR_LHU || instr == INSTR_SB|| instr == INSTR_SH || instr == INSTR_SW || instr == INSTR_AUIPC)
			return ALU_ADD;
		else if (instr == INSTR_SUB || instr == INSTR_BEQ|| instr == INSTR_BNE)
			return ALU_SUB;
		else if (instr == INSTR_SLL || instr == INSTR_SLLI)
			return ALU_SLL;
		else if (instr == INSTR_SLT || instr == INSTR_SLTI || instr == INSTR_BLT || instr == INSTR_BGE)
			return ALU_SLT;
		else if (instr == INSTR_SLTU || instr == INSTR_SLTUI || instr == INSTR_BLTU || instr == INSTR_BGEU)
			return ALU_SLTU;
		else if (instr == INSTR_XOR || instr == INSTR_XORI)
			return ALU_XOR;
		else if (instr == INSTR_SRL || instr == INSTR_SRLI)
			return ALU_SRL;
		else if (instr == INSTR_SRA || instr == INSTR_SRAI)
			return ALU_SRA;
		else if (instr == INSTR_OR || instr == INSTR_ORI || instr == INSTR_CSRRS || instr == INSTR_CSRRSI)
			return ALU_OR;
		else if (instr == INSTR_AND || instr == INSTR_ANDI)
			return ALU_AND;
		else if (instr == INSTR_JALR || instr == INSTR_JAL)
			return ALU_XOR;
		else if (instr == INSTR_LUI)
			return ALU_COPY1;
		else if (instr == INSTR_CSRRW || instr == INSTR_CSRRWI)
			return ALU_CSRRW;
     	else if (instr == INSTR_CSRRC || instr == INSTR_CSRRCI)
			return ALU_CSRRC;
		else return ALU_X;
	endfunction: get_alu_funct;

	function logic[31:0] get_alu_result(e_ALUfuncType alu_function, logic[31:0] aluOp1, logic[31:0] aluOp2);
		if (alu_function == ALU_ADD)
			return (aluOp1 + aluOp2);
     	else if (alu_function == ALU_SUB)
			return (aluOp1 + (-aluOp2));
     	else if (alu_function == ALU_AND)
			return (aluOp1 & aluOp2);
     	else if (alu_function == ALU_OR)
			return (aluOp1 | aluOp2);
     	else if (alu_function == ALU_XOR)
			return (aluOp1 ^ aluOp2);
     	else if (alu_function == ALU_SLT) begin
			if (signed'(32'(aluOp1)) < signed'(32'(aluOp2)))
				return 1;
         	else
				return 0;
		end
     	else if (alu_function == ALU_SLTU) begin
			if (aluOp1 < aluOp2)
				return 1;
         	else
				return 0;
		end
    	else if (alu_function == ALU_SLL)
			return (aluOp1 << (aluOp2 & 'h1F));
     	else if (alu_function == ALU_SRA)
			return (unsigned'(32'(signed'(32'(aluOp1)) >>> signed'(32'(aluOp2 & 'h1F)))));
     	else if (alu_function == ALU_SRL)
			return (aluOp1 >> (aluOp2 & 'h1F));
     	else if (alu_function == ALU_COPY1)
			return aluOp1;
        else if (alu_function == ALU_CSRRW)
			return aluOp1;
        else if (alu_function == ALU_CSRRC)
			return aluOp2 & ((-aluOp1) - 1);
     	else
			return 0;
	endfunction: get_alu_result;

	function logic[31:0] get_immediate(logic[31:0] encodedInstr);
		logic[6:0] opcode;
		opcode = encodedInstr[6:0];
		if (opcode == OPCODE_I_I || opcode == OPCODE_I_L || opcode == OPCODE_I_J || opcode == OPCODE_I_M || opcode == OPCODE_I_S) begin
			if (encodedInstr[31] == 0)
				return ({20'h00000, encodedInstr[31:20]});
			else
				return ({20'hFFFFF, encodedInstr[31:20]});
		end
     		else if (opcode == OPCODE_S) begin
			if (encodedInstr[31] == 0)
				return ({20'h00000, encodedInstr[31:25], encodedInstr[11:7]});
			else
				return ({20'hFFFFF, encodedInstr[31:25], encodedInstr[11:7]});
		end
     		else if (opcode == OPCODE_B) begin
			if (encodedInstr[31] == 0)
				return ({20'h00000, encodedInstr[7], encodedInstr[30:25], encodedInstr[11:8], 1'b0});
			else
				return ({20'hFFFFF, encodedInstr[7], encodedInstr[30:25], encodedInstr[11:8], 1'b0});
		end
     		else if (opcode == OPCODE_U1 || opcode == OPCODE_U2) begin
				return ({encodedInstr[31:12],12'h000});
		end
     		else if (opcode == OPCODE_J) begin
			if (encodedInstr[31] == 0)
				return ({12'h000, encodedInstr[19:12], encodedInstr[20], encodedInstr[30:21], 1'b0});
			else
				return ({12'hFFF, encodedInstr[19:12], encodedInstr[20], encodedInstr[30:21], 1'b0});
		end
     		else
			return 0;
	endfunction: get_immediate;


	function e_ME_MaskType get_memory_mask(e_InstrType_Complete instr);
		if (instr == INSTR_LB || instr == INSTR_SB)
			return MT_B;
     		else if (instr == INSTR_LH || instr == INSTR_SH)
			return MT_H;
    		else if (instr == INSTR_LW || instr == INSTR_SW)
			return MT_W;
     		else if (instr == INSTR_LBU)
			return MT_BU;
     		else if (instr == INSTR_LHU)
			return MT_HU;
     		else
			return MT_X;
	endfunction: get_memory_mask;

	function logic[31:0] branch_PC_calculation(e_InstrType_Complete instr_type, logic[31:0] aluResult, logic[31:0] pcReg, logic[31:0] imm);
		if (instr_type == INSTR_BEQ && aluResult == 0)
			return pcReg + imm;
		else if (instr_type == INSTR_BNE && aluResult != 0)
			return pcReg + imm;
		else if (instr_type == INSTR_BLT && aluResult == 1)
			return pcReg + imm;
		else if (instr_type == INSTR_BGE && aluResult == 0)
			return pcReg + imm;
		else if (instr_type == INSTR_BLTU && aluResult == 1)
			return pcReg + imm;
		else if (instr_type == INSTR_BGEU && aluResult == 0)
			return pcReg + imm;
		else
			return pcReg + 4;
	endfunction: branch_PC_calculation;


	function logic[31:0] get_ENC_U_ALU_result(logic[31:0] imm, logic[31:0] pcReg, e_InstrType_Complete instr_type);
		if (instr_type == INSTR_LUI)
			return get_alu_result(ALU_COPY1, imm, 0);
     	else
			return get_alu_result(ALU_ADD, pcReg, imm);
	endfunction: get_ENC_U_ALU_result


	function logic[31:0] get_CSR(st_CSRfileType csrfile, logic[31:0] encodedInstr);
		if(MCSR_MSTATUS == {20'h00000, encodedInstr[31:20]}) begin
			return csrfile.mstatus;
		end
		else if(MCSR_MIE == {20'h00000, encodedInstr[31:20]}) begin
			return csrfile.mie;
		end
		else if(MCSR_MTVEC == {20'h00000, encodedInstr[31:20]}) begin
			return csrfile.mtvec;
		end
		else if(MCSR_MEPC == {20'h00000, encodedInstr[31:20]}) begin
			return csrfile.mepc;
		end
		else if(MCSR_MCAUSE == {20'h00000, encodedInstr[31:20]}) begin
			return csrfile.mcause;
		end
		else if(MCSR_MTVAL == {20'h00000, encodedInstr[31:20]}) begin
			return csrfile.mtval;
		end
		else if(MCSR_MISA == {20'h00000, encodedInstr[31:20]}) begin
			return csrfile.misa;
		end
		else if(MCSR_MSCRATCH == {20'h00000, encodedInstr[31:20]}) begin
			return csrfile.mscratch;
		end
		else return 0;
	endfunction: get_CSR

	function logic[31:0] get_CSR_result(e_InstrType_Complete instr, logic[31:0] rs1, logic[31:0] csr);
		if (instr == INSTR_CSRRW || instr == INSTR_CSRRWI)
			return rs1;
		else if (instr == INSTR_CSRRS || instr == INSTR_CSRRSI)
			return csr | rs1;
     		else if (instr == INSTR_CSRRC || instr == INSTR_CSRRCI)
			return csr & ((-rs1) - 1);
     		else return 0;
	endfunction: get_CSR_result

	function logic[31:0] update_PC(st_CSRfileType csrfile, logic[31:0] pcReg, bit excFlag);
	    if (excFlag) begin
				return csrfile.mtvec;
			end
			else if (MSTATUS_MIE(csrfile.mstatus) != 0) begin
				if ((MTRAP_MEI(csrfile.mie) != 0) && (MTRAP_MEI(csrfile.mip) != 0)) begin
					return csrfile.mtvec;
				end
	      else if ((MTRAP_MSI(csrfile.mie) != 0) && (MTRAP_MSI(csrfile.mip) != 0)) begin
					return csrfile.mtvec;
				end
	      else if ((MTRAP_MTI(csrfile.mie) != 0) && (MTRAP_MTI(csrfile.mip) != 0)) begin
					return csrfile.mtvec;
				end
	      else return pcReg;
		end
		else return pcReg;
	endfunction: update_PC

	function logic[31:0] update_MCAUSE(st_CSRfileType csrfile, bit excFlag);
		if (excFlag) begin
			return csrfile.mcause;
		end
		else if (MSTATUS_MIE(csrfile.mstatus) != 0) begin
			if ((MTRAP_MEI(csrfile.mie) != 0) && (MTRAP_MEI(csrfile.mip) != 0)) begin
				return 32'h8000000B;
			end
      else if ((MTRAP_MSI(csrfile.mie) != 0) && (MTRAP_MSI(csrfile.mip) != 0)) begin
				return 32'h80000003;
			end
      else if ((MTRAP_MTI(csrfile.mie) != 0) && (MTRAP_MTI(csrfile.mip) != 0)) begin
				return 32'h80000007;
			end
      else return csrfile.mcause;
		end
      else return csrfile.mcause;
	endfunction: update_MCAUSE

	function logic[31:0] update_MSTATUS(st_CSRfileType csrfile, bit excFlag);
		if (excFlag) begin
		return csrfile.mstatus & (unsigned'(32'hFFFFFFF7));
		end
		else if (MSTATUS_MIE(csrfile.mstatus) != 0) begin
			if ((MTRAP_MEI(csrfile.mie) != 0) && (MTRAP_MEI(csrfile.mip) != 0)) begin
				return csrfile.mstatus & (unsigned'(32'hFFFFFFF7));
			end
			else if ((MTRAP_MSI(csrfile.mie) != 0) && (MTRAP_MSI(csrfile.mip) != 0)) begin
					return csrfile.mstatus & (unsigned'(32'hFFFFFFF7));
			end
			else if ((MTRAP_MTI(csrfile.mie) != 0) && (MTRAP_MTI(csrfile.mip) != 0)) begin
					return csrfile.mstatus & (unsigned'(32'hFFFFFFF7));
			end
			else return csrfile.mstatus;
		end
		else return csrfile.mstatus;
	endfunction: update_MSTATUS

	function logic[31:0] update_MEPC(st_CSRfileType csrfile, logic[31:0] pcReg, bit excFlag);
        if (excFlag) begin
					return csrfile.mepc;
				end
				else if (MSTATUS_MIE(csrfile.mstatus) != 0) begin
					if ((MTRAP_MEI(csrfile.mie) != 0) && (MTRAP_MEI(csrfile.mip) != 0)) begin
						return pcReg;
					end
		      else if ((MTRAP_MSI(csrfile.mie) != 0) && (MTRAP_MSI(csrfile.mip) != 0)) begin
						return pcReg;
					end
		      else if ((MTRAP_MTI(csrfile.mie) != 0) && (MTRAP_MTI(csrfile.mip) != 0)) begin
						return pcReg;
					end
		      else return csrfile.mepc;
				end
     		else return csrfile.mepc;
	endfunction: update_MEPC
endpackage
