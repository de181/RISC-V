#!/bin/bash

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     tool=libtoolize;;
    Darwin*)    tool=glibtoolize;;
    CYGWIN*)    echo "CYGWIN not tested - Please contact dev team" && exit;;
    MINGW*)     echo "MINGW not tested - Please contact dev team" && exit;;
    *)          echo "UNKNOWN environment:${unameOut} - Please contact dev team" && exit;;
esac

cd ../3rdParty/systemc/2.3.3/src/SYSTEMC && rm -f aclocal.m4 && aclocal && ${tool} --force && automake --add-missing && autoreconf