include(ExternalProject)

set(BOOST_ROOT_DIR "${CMAKE_EXTERNAL_PROJECT_DIR}/boost/src")
set(BOOST_SRC_DIR "${BOOST_ROOT_DIR}/BOOST")

ExternalProject_Add(BOOST
  PREFIX ${CMAKE_EXTERNAL_PROJECT_DIR}/boost
  URL https://boostorg.jfrog.io/artifactory/main/release/1.78.0/source/boost_1_78_0.tar.gz

  CONFIGURE_COMMAND ""
  BUILD_COMMAND cd "${BOOST_SRC_DIR}" && sh bootstrap.sh
  INSTALL_COMMAND cd "${BOOST_SRC_DIR}" && ./b2 --prefix=../BOOST-build --exec-prefix=../BOOST-build --libdir=../BOOST-build --includedir=../BOOST-build --build-dir=${BOOST_ROOT_DIR}/BOOST-build/tmp --with-iostreams --with-program_options --no-cmake-config install
  )

set(BOOST_INCLUDE_DIR ${BOOST_SRC_DIR})
set(BOOST_LIB_DIR "${BOOST_ROOT_DIR}/BOOST-build")
