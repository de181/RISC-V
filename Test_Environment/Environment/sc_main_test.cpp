// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Contact: contact@lubis-eda.com
// Author: Luis Rivas, lukadejanovic, Sandeep Ragipati
// -------------------------------------------------

#include <iostream>
#include <chrono>

//#define USE_ELF 1
// USE_ELF = 1 -> This configuration sets the acceptable input programs types to elf.
//                  Also, it will require the use of the memory module defined in ELFstyle subdirectory.
// USE_ELF = 0 -> This configuration sets the acceptable input programs types to hex.
//                  Also, it will require the use of the memory module defined in current directory.

#include "Core_test.h"


int sc_main(int argc, char *argv[]) {
    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::duration;
    using std::chrono::milliseconds;

    if (argc == 2) {
        Core_test *test_object = new Core_test(argc, argv);
        auto t1 = high_resolution_clock::now();
        test_object->perform_test();
        auto t2 = high_resolution_clock::now();
        duration<double, std::milli> ms_double = t2 - t1;
        std::cout << ms_double.count() << "ms";
        return 0;
    } else {
        std::cout << "You must specify the elf file path as the first argument Terminating..." << endl;
        return 1;
    }
}