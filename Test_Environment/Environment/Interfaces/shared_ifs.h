// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Contact: contact@lubis-eda.com
// Author: Luis Rivas, Nawras Altaleb, Sandeep Ragipati
// -------------------------------------------------

#ifndef INTERFACES_SHARED_IFS
#define INTERFACES_SHARED_IFS

template<typename T>
class shared_in_if : virtual public sc_interface
{
   public:
      virtual  void get(T & out) = 0;
};

template<typename T> 
class shared_out_if : virtual public sc_interface
{
   public:
      virtual void set(const T & val) = 0;
}; 

#endif //INTERFACES_SHARED_IFS
