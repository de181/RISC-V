# -------------------------------------------------
# Copyright(c) LUBIS EDA GmbH, All rights reserved
# Contact: contact@lubis-eda.com
# Author: Luis Rivas, Sandeep Ragipati
# -------------------------------------------------

set SCRIPT_LOCATION [file dirname [file normalize [info script]]];
set RTL_PATH $SCRIPT_LOCATION/RTL;
set PROP_PATH $SCRIPT_LOCATION/SVA;

clear -all;

analyze -sv12 ${RTL_PATH}/globalTypes.sv;
analyze -sv12 ${RTL_PATH}/ISS_Interrupts_types.sv;
analyze -sv12 ${RTL_PATH}/ISS_Interrupts.sv;

analyze -sv12 ${PROP_PATH}/SVA_Properties_JasperGold/ISS_Interrupts_binding.sv;

elaborate;

clock clk ;
reset -clear; reset -none;
assume -reset rst
set_loop_handling assume_stable
prove -all
