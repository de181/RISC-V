// -------------------------------------------------
// Copyright(c) LUBIS EDA GmbH, All rights reserved
// Contact: contact@lubis-eda.com
// Author: Luis Rivas, Nawras Altaleb, Sandeep Ragipati
// -------------------------------------------------

#ifndef PROJECT_CPU_INTERFACES_H
#define PROJECT_CPU_INTERFACES_H

#include "../Test_Environment/Environment/RISCV_commons/Memory_Interfaces.h"

enum EncType {
    ENC_B, ENC_ERR, ENC_I_I, ENC_I_J, ENC_I_L, ENC_I_M, ENC_I_S, ENC_J, ENC_R, ENC_S, ENC_U
};

enum ALUfuncType {
    ALU_ADD, ALU_AND, ALU_COPY1, ALU_CSRRW, ALU_CSRRS, ALU_CSRRC, ALU_OR, ALU_SLL, ALU_SLT, ALU_SLTU, ALU_SRA, ALU_SRL, ALU_SUB, ALU_X, ALU_XOR
};


// register file input interface
struct RegfileWriteType {
    unsigned int dst;
    unsigned int dstData;
};

struct RegfileECallType {
    unsigned int reg_file_10;
    unsigned int reg_file_11;
    unsigned int reg_file_12;
    unsigned int reg_file_17;
};

struct CSRfileType {
    unsigned int mcause;
    unsigned int mepc;
    unsigned int mie;
    unsigned int mip;
    unsigned int mstatus;
    unsigned int mtvec;
    unsigned int mtval;
    unsigned int mscratch;
    unsigned int misa = 0x40000080;
};

#endif // PROJECT_CPU_INTERFACES_H
